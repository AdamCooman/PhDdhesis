% adapted the code to work with the MScreate function and the ADSsimulateLSSS function

clear all
close all
clc
cd('C:\users\adam.cooman\Google Drive\PhDthesis\thesis\chapter7_appendices\Appendix2_LSSS\matlabcode\FirstOrderLTP')

% generate the multisine that sets the time-variation
MSdef = MScreate(0.5,5,'DC',1);
MSdef.MSnode = {'sched'};

% alpha=10
% fsys=0.5

% generate a random phase vector
phase = 2*pi*rand(length(MSdef.grid),1,1);

% perform the LSSS simulation in ADS 
[res_kryl,simresult] = ADSsimulateLSSS('firstOrderSystemSimulation_SDD.net',MSdef,'phase',phase,...
    'Start',0.1,'Stop',100,'Dec',100,'LSSSnode','in','Rsource',0,'Krylov',true,'cleanupfiles',false);
% extract the HTFs from the simulation result
[LSSSresult_kryl,~,ssfreq] = LSSSextractHTFs(res_kryl);
% for this comparison, we are only interested in the output of the system
HTFs_ADS_KRYL = squeeze(LSSSresult_kryl.out(1,:,:));

% the number of HTFs is given by the size of the vector
numHTFs = (size(HTFs_ADS_KRYL,1)-1)/2;

pause(1)

% perform the LSSS simulation in ADS 
[res,simresult] = ADSsimulateLSSS('firstOrderSystemSimulation_SDD.net',MSdef,'phase',phase,...
    'Start',0.1,'Stop',100,'Dec',100,'LSSSnode','in','Rsource',0,'Krylov',false,'GuardThreshZero',false);
% extract the HTFs from the simulation result
[LSSSresult,~,ssfreq] = LSSSextractHTFs(res);
% for this comparison, we are only interested in the output of the system
HTFs_ADS = squeeze(LSSSresult.out(1,:,:));


% the number of HTFs is given by the size of the vector
numHTFs = (size(HTFs_ADS,1)-1)/2;

%% check the results from ADS by calculating the HTFs theoretically

% get the variation of the A parameter from the simulation results
A = squeeze((simresult.sched(1,1,:)));

fsys = MSdef.f0;

% a and b coefficients of differential equation, as they change over time
Nharm = max(MSdef.grid);
a = [fliplr(conj(A(2:Nharm+1)).')/2 1 A(2:Nharm+1).'/2
     zeros(1,Nharm) 1 zeros(1,Nharm)];
b = [zeros(1,Nharm) 1 zeros(1,Nharm)];

% calculate the HTFs with Ebrahim's function
HTFs = HTFmat(a,b,fsys,ssfreq,numHTFs+100);

% keep only the wanted HTFs, throw away the others
HTFs = HTFs(:,(end-1)/2+1+(-numHTFs:numHTFs));

% and put Ebrahim's HTFs into my format
HTFs = flipud(HTFs.');


%% plot the results of both methods and compare them.
% 
% HTFstoPlot = -25:5:25;
% 
% figure(9842)
% clf
% subplot(121)
% plotHTFs(db(HTFs((end-1)/2+HTFstoPlot,:)),log10(ssfreq),'b');
% hold on
% plotHTFs(db(HTFs_ADS((end-1)/2+HTFstoPlot,:)),log10(ssfreq),'r');
% plotHTFs(db(HTFs((end-1)/2+HTFstoPlot,:)-HTFs_ADS((end-1)/2+HTFstoPlot,:)),log10(ssfreq),'k');
% 
% 
% subplot(122)
% plotHTFs(db(HTFs((end-1)/2+HTFstoPlot,:)),log10(ssfreq),'b');
% hold on
% plotHTFs(db(HTFs_ADS_KRYL((end-1)/2+HTFstoPlot,:)),log10(ssfreq),'r');
% plotHTFs(db(HTFs((end-1)/2+HTFstoPlot,:)-HTFs_ADS_KRYL((end-1)/2+HTFstoPlot,:)),log10(ssfreq),'Color',[0.7 0.7 0.7]);

%% plot again, but now in a flat thing

HTFstoPlot = -40:5:40;

figure(2165)
clf
hold on
plot(log10(ssfreq),db(HTFs( (end-1)/2+HTFstoPlot , : )),'-','Color',[0 0.7 0],'LineWidth',1)
plot(log10(ssfreq),db(HTFs_ADS_KRYL( (end-1)/2+HTFstoPlot , : )),'--','Color',[0 0.3 0],'LineWidth',1)
plot(log10(ssfreq),db(HTFs((end-1)/2+HTFstoPlot,:)-HTFs_ADS_KRYL((end-1)/2+HTFstoPlot,:)),'r','LineWidth',1)
grid on
set(gca,'XTick',[-1 0 1 2],'XTickLabel',{'0.1','1','10','100'})
set(gca,'YTick',[-400 -300 -200 -100 0]);
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none')
ylabel('HTF magintude $\left[ \mathrm{dB} \right]$','interpreter','none')
ylim([-400 0])

cleanfigure
matlab2tikz('HTFerror_kryl.tex','height','\figureheight','width','\figurewidth','parseStrings',false);

figure(18732)
clf
hold on
plot(log10(ssfreq),db(HTFs( (end-1)/2+HTFstoPlot , : )),'-','Color',[0 0.7 0],'LineWidth',1)
plot(log10(ssfreq),db(HTFs_ADS( (end-1)/2+HTFstoPlot , : )),'r--','Color',[0 0.3 0],'LineWidth',1)
plot(log10(ssfreq),db(HTFs((end-1)/2+HTFstoPlot,:)-HTFs_ADS((end-1)/2+HTFstoPlot,:)),'r','LineWidth',1)
grid on
set(gca,'XTick',[-1 0 1 2],'XTickLabel',{'0.1','1','10','100'})
set(gca,'YTick',[-400 -300 -200 -100 0]);
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none')
ylabel('HTF magintude $\left[ \mathrm{dB} \right]$','interpreter','none')
ylim([-400 0])

cleanfigure
matlab2tikz('HTFerror_correct.tex','height','\figureheight','width','\figurewidth','parseStrings',false);


%% make a plot of the scheduling

[signals,time] = ADSconvert2timeDomain(simresult);

figure(846215)
clf
plot(time,squeeze(signals.sched),'-','Linewidth',1,'Color',[0 0.7 0]);
xlabel('Time $\left[ \mathrm{s} \right]$','interpreter','none')
ylabel('a parameter $\left[ \mathrm{a.u.} \right]$','interpreter','none')
grid on

cleanfigure
matlab2tikz('HTFerror_scheduling.tex','height','\figureheight','width','\figurewidth','parseStrings',false);


