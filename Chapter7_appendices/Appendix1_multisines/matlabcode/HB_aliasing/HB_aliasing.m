close all
clear all
clc

netlist = readTextFile('HB_aliasing.net');

%netlist{end+1} = 'Tran:HB1_tran HB_Sol=1 SteadyState=1 Freq[1]=1.0 GHz Order[1]=N OutputPlan="HB1_Output"';
%netlist{end+1} = 'Component:tahb_HB1 Module="ATAHB" Type="ModelExtractor" Tran_Analysis="HB1_tran" HB_Analysis="HB1"';

netlist{end+1} = 'HB:HB1 MaxOrder=0.5*N*(T+1) Freq[1]=1.0 GHz Freq[2]=5 MHz Order[1]=N Order[2]=0.5*N*(T+1)';

writeTextFile(netlist,'temp.net');
HB1 = ADSrunSimulation('temp.net');
HB1 = HB1.sim1_HB;
delete('temp.net')

netlist{end} = 'HB:HB1 MaxOrder=0.5*N*(T+1) Freq[1]=5 MHz Freq[2]=1.0 GHz Order[1]=0.5*N*(T+1) Order[2]=N';

writeTextFile(netlist,'temp.net');
HB2 = ADSrunSimulation('temp.net');
HB2 = HB2.sim1_HB;
delete('temp.net')

%%

figure(1);
clf
hold all
stem(HB1.freq/1e9,db(HB1.I_d+eps*rand(size(HB2.I_d)))+350,'+');
stem(HB2.freq/1e9,db(HB2.I_d+eps*rand(size(HB2.I_d)))+350,'o');
ylim([0 350]);
xlim([0 3.5]);
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none');
ylabel('Current $\left[ \mathrm{dBA} \right]$','interpreter','none');
set(gca,'YTick',[50 150 250 350],'YTickLabel',{'-300' '-200' '-100' '0'})
set(gca,'XTick',[0 1 2 3]);
legend({'$\fcenter$ first','$\fres$ first'},'interpreter','none');

cleanfigure
matlab2tikz('HB_aliasing.tex','width','\figurewidth','height','\figureheight','parseStrings',false);
