% in this demo, I try the different integration methods for envelope
% simulation on the same circuit. To see their influence on the result
% The test circuit consists of a tanh static non-linearity 
% followed by an elliptic filter with center frequency of 10GHz. and a bandwidth of 200MHz
% its transmission zeroes are placed at 9.960GHz and 10.32GHz.
clear variables
close all
clc

% I give the multisine a small DC offset to obtain even-order non-linearities as well
MSdef = MScreate(1e6,10e9,'mode','bandpass','bandwidth',200e6,'numTones',41,'DC',1,'rms',0.2);
MSdef.MSnode = 'in';

% generate a phase vector for the multisine
phase = zeros(length(MSdef.phase),1,1);
phase(:,1,1) = 2*pi*rand(size(MSdef.phase));

system = 'testSystem.net';

% NL is the assumed order of non-linearity in the circuit
NL = 12;


%% Harmonic Balance simulation 

try
    HB = ADSsimulateMS('testSystem.net',MSdef,'simulator','HB','numberOfRealisations',1,'phase',phase,'oversample',NL);
    HB.nl = HB.I_nl;
    save HB HB
catch
    load('HB')
end

fprintf(1,'The amount of frequencies in the HB grid is: %s\n',num2str(length(HB.freq)));

%%

fundBins_HB = find((HB.freq>8e9)&(HB.freq<12e9));
FILTER_HB = squeeze(HB.out(:,end,fundBins_HB))./squeeze(HB.nl(:,end,fundBins_HB));

deteremineFilterFRFandPlot( HB , FILTER_HB, HB.freq(fundBins_HB),'HB');


%% transient sumulation
P = 4;
try
    [TRAN,raw] = ADSsimulateMS('testSystem.net',MSdef,'simulator','TRAN','numberOfRealisations',1,'phase',phase,...
    'periods',P,'oversample',NL);
    TRAN.nl = TRAN.I_nl;
    save TRAN TRAN raw
catch
    load('TRAN')
end

fprintf(1,'2*NL*fmax is: %s\n',eng(2*NL*MSdef.fmax) );
fprintf(1,'The used sample time was: %s\n',eng(raw.sim1_TRAN.time(2)-raw.sim1_TRAN.time(1)) );
fprintf(1,'Which corresponds to a sample frequency of: %s\n',eng(1./(raw.sim1_TRAN.time(2)-raw.sim1_TRAN.time(1))));
fprintf(1,'The amount of returned points in the simulation is:%s\n',num2str(length(raw.sim1_TRAN.time)));

%%
% % show the resulting spectra
% figure(1)
% plot(TRAN.freq/1e9 ,db(squeeze(TRAN.nl(1,end,:))),'+')
% xlabel('Frequency [GHz]');
% ylabel('Amplitude [dBV]');


% show the transient, to demonstrate that we are in steady-state
determineTransient(raw,P,false,'TRAN');

deteremineFilterFRFandPlot( TRAN , FILTER_HB, HB.freq(fundBins_HB),'TRAN');

%% Envelope with TRAP

% simulate the system using trapezoidal integration
try
    [ENV_TRAP,raw] = ADSsimulateMS('testSystem.net',MSdef,'simulator','ENV','numberOfRealisations',1,'phase',phase,...
    'EnvIntegOrder',2,'EnvUseGear',0,'periods',P,'oversample',20);
    ENV_TRAP.nl = ENV_TRAP.I_nl;
    save ENVtrap ENV_TRAP raw
catch
    load('ENVtrap')
end

% show the transient, to demonstrate that we are in steady-state
determineTransient(raw,P,true,'ENVTRAP')

% estimate the FRF of the elliptic filter and plot it
deteremineFilterFRFandPlot( ENV_TRAP , FILTER_HB, HB.freq(fundBins_HB),'ENVTRAP',[7 13]);

% make the custom plot to show the wierd stuff in ENV_TRAP
figure(3)
subplot(121)
determineTransient(raw,P,true,'ENVTRAP',false)
subplot(122)
ylabel('Amplitude $\left[ \mathrm{dBV} \right]$')
set(gca,'YTickLabel',{'-300' '-200' '-100' '0'})

cleanfigure
matlab2tikz('ENVTRAP_inout.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

figure(546842)
plot(ENV_TRAP.freq,db(squeeze(ENV_TRAP.out(1,end,:))),'.');

%% Envelope with GEAR

% simulate the system using gear's order two integration
try
    [ENV_GEAR,raw] = ADSsimulateMS('testSystem.net',MSdef,'simulator','ENV','numberOfRealisations',1,'phase',phase,...
        'EnvIntegOrder',2,'EnvUseGear',1,'periods',P,'oversample',NL,'EnvOversample',NL);
    ENV_GEAR.nl = ENV_GEAR.I_nl;
    save ENVgear ENV_GEAR raw
catch
    load('ENVgear')
end

fprintf(1,'NL*fbw is: %s\n',eng(NL*(MSdef.fmax-MSdef.fmin)) );
fprintf(1,'The used sample time was: %s\n',eng(raw.sim1_HB.sweepvar_time(2)-raw.sim1_HB.sweepvar_time(1)) );
fprintf(1,'Which corresponds to a sample frequency of: %s\n',eng(1./(raw.sim1_HB.sweepvar_time(2)-raw.sim1_HB.sweepvar_time(1))));
fprintf(1,'The amount of returned points in the simulation is:%s\n',num2str(length(raw.sim1_HB.sweepvar_time)));

%%
% show the transient, to demonstrate that we are in steady-state
determineTransient(raw,P,true,'ENVGEAR')

% estimate the FRF of the elliptic filter and plot it
deteremineFilterFRFandPlot( ENV_GEAR , FILTER_HB, HB.freq(fundBins_HB),'ENVGEAR');


