clear all
close all
clc

SIM = ADSrunSimulation('HB_clipping.net');

TRAN = SIM.sim1_TRAN;
HB = SIM.sim2_HB;
% calculate the time-domain response of the HB simulation
[HBtime,time] = ADSconvert2timeDomain(HB);
%%
% plot the different results
figure(1)
clf
hold on
plot(TRAN.time*1e9,TRAN.I_Probe1,'r-');
plot(time*1e9,HBtime.I_Probe1,'b-');
xlim([0 0.5])
ylim([-0.12 0.12])
grid on
legend({'Transient simulation','Harmonic Balance'})
xlabel('Time $\left[ \mathrm{s} \right]$')
ylabel('Signal $\left[ \mathrm{a.u.} \right]$')

cleanfigure
matlab2tikz('HB_clipping.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


figure(2)
clf
hold on
plot(db(fft(HBtime.I_Probe1)));
plot(db(fft(TRAN.I_Probe1)))
