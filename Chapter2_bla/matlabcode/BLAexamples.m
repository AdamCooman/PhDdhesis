% in this script, I determine the BLA of a test system with noise and with
% multisines

% generate the input
u = randn(10000,1);


y = tanh(u);

figure
hist(u)

figure
plot(-2:0.01:2,tanh(-2:0.01:2),'k')