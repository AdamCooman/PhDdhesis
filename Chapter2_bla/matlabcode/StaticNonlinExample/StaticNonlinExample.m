clear all
close all
clc

%% make a plot of the non-linearity

x = linspace(-1,2,301);

figure(165642)
plot(x,NL(x),'Color',[0 0.7 1],'Linewidth',2);
grid on
xlabel('Input value','interpreter','none')
ylabel('Output value','interpreter','none')
ylim([-0.1 1.1])

cleanfigure
matlab2tikz('StaticNL_function.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

%% calculate the BLA for a single RMS value


M = 5000;
P = 10000;

rms = 0.1;

[BLA, varBLA, freq] = runNoiseBLA( rms , M , P , true);

%% make sure the plot of the input and output is nice

figure(163542)

subplot(131)
ylim([-0.4 0.4])
set(gca,'Ytick',[-0.4 -0.2 0 0.2 0.4],'YTickLabel','');
set(gca,'Xtick',[]);
set(gca,'Xticklabel',{});
title('PDF','FontWeight','Normal')
xlabel('')
grid on

subplot(132)
ylim([-0.4 0.4])
set(gca,'Ytick',[-0.4 -0.2 0 0.2 0.4]);
set(gca,'Xticklabel',{});
grid on
title('Time domain signal','FontWeight','Normal')
xlabel('Time','interpreter','none');

subplot(133)
ylim([-140 -40])
set(gca,'Xticklabel',{});
%ylabel('Amplitude $\left[ \mathrm{dB} \right]$','interpreter','none');
title('Spectrum $\left[ \mathrm{dB} \right]$','FontWeight','Normal')
xlabel('Frequency','interpreter','none');
grid on

cleanfigure
matlab2tikz('StaticNL_IN.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


figure(584654)
subplot(131)
ylim([0 0.4])
set(gca,'Ytick',[0 0.1 0.2  0.3 0.4],'YTickLabel','');
set(gca,'Xtick',[]);
title('PDF','FontWeight','Normal')
grid on

subplot(132)
ylim([0 0.4])
set(gca,'Ytick',[0 0.1 0.2 0.3 0.4]);
set(gca,'Xticklabel',{});
xlabel('Time','interpreter','none');
grid on
title('Time domain signal','FontWeight','Normal')

subplot(133)
ylim([-140 -40])
xlabel('Frequency','interpreter','none');
set(gca,'Xticklabel',{});
%ylabel('Amplitude $\left[ \mathrm{dB} \right]$','interpreter','none');
title('Spectrum [dB]','FontWeight','Normal')
grid on

cleanfigure
matlab2tikz('StaticNL_OUT.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



%% plot the BLA nicely

bins = 1:1:length(BLA);

figure(516542)
clf
hold on
plot(freq(bins),abs(BLA(bins)),'Linewidth',2,'Color',[0 0.7 0]);
fill([freq(bins) fliplr(freq(bins))],[abs(BLA(bins))+3*sqrt(varBLA(bins)) fliplr(abs(BLA(bins))-3*sqrt(varBLA(bins)))],...
    [0 0.7 0],'EdgeColor','none','FaceAlpha',0.5);
xlim([0 1]);
ylim([0.33 0.39])
grid on
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \norm{\cdot} \right]$','interpreter','none');

cleanfigure
matlab2tikz('StaticNL_BLA.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

%% estimate the same BLA with a multisine

M = 2000;

MSdef = MScreate(1/500,1,'rms',0.1,'DC',0.05);
for mm=1:M
    % give the multisine a new phase
    MSdef.phase = 2*pi*rand(size(MSdef.grid));
    % calculate the time-domain signal of the multisine
    if mm==1
        [t,time] = MScalculatePeriod(MSdef,20);
        ms = zeros(M,1,length(time));
        ms(mm,1,:) = t;
    else
        ms(mm,1,:) = MScalculatePeriod(MSdef,20);
    end
end

% calculate the spectrum of the multisine as well
spec.ref = fft(ms,[],3)/size(ms,3);
spec.ref = spec.ref(:,:,1:end/2);
spec.freq = (0:size(spec.ref,3)-1)*MSdef.f0;

figure(2135468)
clf

subplot(131)
[counts,bins] = hist(squeeze(ms(1,1,:)),50);
h=barh(bins,counts);
set(get(h,'Parent'),'xdir','r')
ylim([-0.4 0.4])
grid on
set(gca,'Xtick',[]);
set(gca,'Ytick',[-0.4 -0.2 0 0.2 0.4],'YTickLabel','');
title('PDF','FontWeight','Normal')
xlabel('')

subplot(132)
plot(time,squeeze(ms(1,1,:)));
ylim([-0.4 0.4])
grid on
set(gca,'Ytick',[-0.4 -0.2 0 0.2 0.4]);
grid on
set(gca,'xticklabel',{});
title('Time domain signal','FontWeight','Normal')
xlabel('Time','interpreter','none');

subplot(133)
plot(spec.freq,db(squeeze(spec.ref(1,1,:))),'k.');
xlim([0 10]);
ylim([-140 -40])
xlabel('Frequency','interpreter','none');
set(gca,'xticklabel',{});
% ylabel('Amplitude $\left[ \mathrm{dB} \right]$','interpreter','none');
title('Spectrum $\left[ \mathrm{dB} \right]$','FontWeight','Normal')
grid on


cleanfigure
matlab2tikz('StaticNL_MS_IN.tex','width','\figurewidth','height','\figureheight','parseStrings',false);





%% calculate the response of the non-linear system
y = NL(ms);

% calculate the spectrum as well
spec.out = fft(y,[],3)/size(y,3);
spec.out = spec.out(:,:,1:end/2);
exBins = MSdef.grid+1;
nexBins = setdiff(1:length(spec.freq),exBins);

figure(654321)
clf

subplot(131)
[counts,bins] = hist(squeeze(y(1,1,:)),50);
h=barh(bins,counts);
set(get(h,'Parent'),'xdir','r')
ylim([0 0.4])
grid on
set(gca,'Xtick',[]);
set(gca,'Ytick',[0 0.1 0.2 0.3 0.4],'YTickLabel','');
title('PDF','FontWeight','Normal')
xlabel('')

subplot(132)
plot(time,squeeze(y(1,1,:)));
ylim([0 0.4])
grid on
set(gca,'Ytick',[0 0.1 0.2 0.3 0.4]);
grid on
title('Time domain signal','FontWeight','Normal')
xlabel('Time','interpreter','none');
set(gca,'xticklabel',{});

subplot(133)
hold on
plot(spec.freq(exBins),db(squeeze(spec.out(1,1,exBins))),'k.','markersize',1);
plot(spec.freq(nexBins),db(squeeze(spec.out(1,1,nexBins))),'m.','markersize',1);
xlim([0 10]);
ylim([-140 -40])
xlabel('Frequency','interpreter','none');
% ylabel('Amplitude ','interpreter','none');
title('Spectrum $\left[ \mathrm{dB} \right]$','FontWeight','Normal')
set(gca,'xticklabel',{});
grid on


cleanfigure
matlab2tikz('StaticNL_MS_OUT.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



%% and calculate the BLA
G = calculateSISO_BLA(spec,'input','ref','output','out','reference','ref');


figure(554213)
clf
hold on
plot(G.freq,abs(G.mean),'Linewidth',2,'Color',[0 0.7 0]);
fill([G.freq fliplr(G.freq)],[abs(G.mean)+3*G.stdNL fliplr(abs(G.mean)-3*G.stdNL)],...
    [0 0.7 0],'EdgeColor','none','FaceAlpha',0.5);
xlim([0 1]);
ylim([0.33 0.39])
grid on
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \norm{\cdot} \right]$','interpreter','none');

cleanfigure
matlab2tikz('StaticNL_MS_BLA.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



%% use a random-odd multisine

M = 100;

MSdef = MScreate(MSdef.f0,MSdef.fmax,'grid','random-odd','rms',0.1,'DC',0.05);
for mm=1:M
    % give the multisine a new phase
    MSdef.phase = 2*pi*rand(size(MSdef.grid));
    % calculate the time-domain signal of the multisine
    if mm==1
        [t,time] = MScalculatePeriod(MSdef,20);
        ms = zeros(M,1,length(time));
        ms(mm,1,:) = t;
    else
        ms(mm,1,:) = MScalculatePeriod(MSdef,20);
    end
end
% calculate the spectrum of the multisine as well
spec.ref = fft(ms,[],3)/size(ms,3);
spec.ref = spec.ref(:,:,1:end/2);
spec.freq = (0:size(spec.ref,3)-1)*MSdef.f0;

y = NL(ms);

% calculate the spectrum as well
spec.out = fft(y,[],3)/size(y,3);
spec.out = spec.out(:,:,1:end/2);

% get all them bins
exBins = MSdef.grid+1;
evenBins = 1:2:size(spec.out,3);
oddBins = setdiff(2:2:size(spec.out,3),exBins);

figure(213542)
clf
hold on
plot( spec.freq(evenBins) , db(squeeze(spec.out(1:10,1,evenBins))) , 'b.' , 'markersize',1);
plot( spec.freq(oddBins ) , db(squeeze(spec.out(1:10,1,oddBins ))) , 'r.' , 'markersize',1);
plot( spec.freq(exBins  ) , db(squeeze(spec.out(1:10,1,exBins  ))) , 'k.' , 'markersize',1);
xlim([0 5])
ylim([-140 -40])
grid on
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none');
ylabel('Amplitude $\left[ \mathrm{dB} \right]$','interpreter','none');

cleanfigure
matlab2tikz('StaticNL_OddMS.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



%% sweep the RMS power and determine the BLA


M = 5000;
P = 10000;

rms = logspace(-2,0,40);


for ind=1:length(rms)
    disp(['rms = ' num2str(rms(ind))])
    if ind==1
        [t, vart] = runNoiseBLA( rms(ind) , M , P );
           BLA = zeros(length(rms),length(t));
        varBLA = zeros(length(rms),length(t));
           BLA(ind,:) = t;
        varBLA(ind,:) = vart;
    else
        [BLA(ind,:), varBLA(ind,:)] = runNoiseBLA( rms(ind) , M , P );
    end
end
disp('done')

% plot the 
% 
% figure(3546354)
% clf
% hold on
% plot(1:length(BLA),abs(BLA),'Linewidth',2,'Color',[0 0 1]);
% plot(1:length(BLA),abs(BLA)+3*sqrt(varBLA),'Linewidth',1,'Color',[0 0.7 1]);
% plot(1:length(BLA),abs(BLA)-3*sqrt(varBLA),'Linewidth',1,'Color',[0 0.7 1]);

%% plot the magnitude of the BLA as a function of the RMS power

bin = 200;

XTick = [(0.01:0.01:0.1) (0.2:0.1:1)];
XTickLabel = repmat({''},length(XTick),1);
XTickLabel{1} = '0.01';
XTickLabel{10} = '0.1';
XTickLabel{end} = '1';

figure(582134)
clf
plot(log10(rms),abs(BLA(:,bin)),'Linewidth',2,'Color',[0 0.7 0]);
hold on
fill(log10([rms fliplr(rms)]),[(abs(BLA(:,bin))+3*sqrt(varBLA(:,bin))).' fliplr((abs(BLA(:,bin))-3*sqrt(varBLA(:,bin))).')],...
    [0 0.7 0],'EdgeColor','none','FaceAlpha',0.5);
set(gca,'XTick',log10(XTick),'XTickLabel',XTickLabel)
set(gca,'YTick',[0.3:0.1:0.6])
grid on
xlabel('rms power of the input signal');
ylabel('Magnitude $\left[ \norm{\cdot} \right]$','interpreter','none');


%% generate the same graph with a sine excitation and plot it on top

t = 0:0.0001:1-0.0001;

for ind=1:length(rms)
    x = 0.05+2*rms(ind)/sqrt(2)*sin(2*pi*2*t);
    disp(['wanted rms: ' num2str(rms(ind))]);
    disp(['calculated rms: ' num2str( sqrt(mean((x-0.05).^2)) )]);
    y = NL(x);
    Y = fft(y);
    X = fft(x);
    GAIN(ind) = Y(3)./X(3);
end

figure(582134)
plot(log10(rms),abs(GAIN),'linewidth',2,'Color',[1 0.7 0]);

cleanfigure
matlab2tikz('StaticNL_BLA_RMS.tex','width','\figurewidth','height','\figureheight','parseStrings',false);
 


