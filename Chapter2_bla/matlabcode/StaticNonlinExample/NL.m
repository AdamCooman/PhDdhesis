function [ y ] = NL( x , NLtype)
%NL applies a static non-linear function to the data
%   
%   y = NL(x)
%
% x is a scalar, vector or matrix of data
% y is the response of the static non-linearity to x


if ~exist('NLtype','var')
    NLtype = 'TRAN';
end

% now apply the non-linearity to the data
switch NLtype
    case 'TRAN'
        y = 3*x.^2-2*x.^3;
        y(x>=1)=1;
        y(x<0)=0;
    otherwise
        error('unknown NL type')
end
    

end

