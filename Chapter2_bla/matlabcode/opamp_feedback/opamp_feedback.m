% this script generates the figures for example 1 in the paper
close all
clc
clear variables

MSdef = MScreate(1e3,1000e3,'rms',0.1,'grid','random-odd');
MSdef.MSnode = {'MS_node','sgnd'};

simres = ADSsimulateMS('MillerOpamp.net',MSdef,'simulator','TRAN','numberOfRealisations',6);
acres = ADSsimulateAC('MillerOpamp.net',MSdef,'Start',1,'Stop',1e9,'Dec',20);


[signals,time] = ADSconvert2timeDomain(simres);

figure(879432)
plot(time,squeeze(signals.out));

%%
close all

maxFreq = 3.5*MSdef.fmax;
[~,maxBin] = min(abs(simres.freq-maxFreq));

exBins = MSdef.grid+1;
evBins = 1:2:maxBin;
odBins = 2:2:maxBin;
for ii=1:length(exBins);
    odBins(odBins==exBins(ii)) = NaN;
end
odBins(isnan(odBins))=[];

% 
ylimits = [-140 -20];
yticks = [-140 -100 -60 -20];
xlimits = [0 simres.freq(maxBin)]/1e6;
xticks = [0 1 2 3];

figure('name','ref')
clf
plot(simres.freq(exBins)/1e6,db(squeeze(simres.MS_node(1,1,exBins))),'k.','markersize',1)
ylim(ylimits);
xlim(xlimits);
set(gca,'YTick',yticks);
set(gca,'XTick',xticks,'XTickLabel',{'0','$1\mathrm{MHz}$','$2\mathrm{MHz}$','$3\mathrm{MHz}$'});
grid on
title('Reference $\left[ \mathrm{dBV} \right]$','interpreter','none');

cleanfigure
matlab2tikz('MILLER_REF.tex','height','\figureheight','width','\figurewidth','parseStrings',false);


figure('name','inout')
clf
% plot the input signal
subplot(121)
plot(simres.freq(exBins)/1e6,db(squeeze(simres.amp(1,1,exBins))),'k.','markersize',1)
hold on
plot(simres.freq(evBins)/1e6,db(squeeze(simres.amp(1,1,evBins))),'b.','markersize',1)
plot(simres.freq(odBins)/1e6,db(squeeze(simres.amp(1,1,odBins))),'r.','markersize',1)
ylim(ylimits);
xlim(xlimits);
set(gca,'YTick',yticks);
set(gca,'XTick',xticks,'XTickLabel',{'0','$1\mathrm{MHz}$','$2\mathrm{MHz}$','$3\mathrm{MHz}$'});
grid on
title('Input voltage $\left[ \mathrm{dBV} \right]$','interpreter','none');
% plot the output signal
subplot(122)
plot(simres.freq(exBins)/1e6,db(squeeze(simres.out(1,1,exBins))),'k.','markersize',1)
hold on
plot(simres.freq(evBins)/1e6,db(squeeze(simres.out(1,1,evBins))),'b.','markersize',1)
plot(simres.freq(odBins)/1e6,db(squeeze(simres.out(1,1,odBins))),'r.','markersize',1)
ylim(ylimits);
xlim(xlimits);
set(gca,'YTick',yticks);
set(gca,'XTick',xticks,'XTickLabel',{'0','$1\mathrm{MHz}$','$2\mathrm{MHz}$','$3\mathrm{MHz}$'});
title('Output voltage $\left[ \mathrm{dBV} \right]$','interpreter','none');
grid on

cleanfigure
matlab2tikz('MILLER_INOUT.tex','height','\figureheight','width','\figurewidth','parseStrings',false);


%% 

[G,Y,U] = calculateSISO_BLA(simres,'input','amp','output','out','reference','MS_node');

temp = 1:1:9;
XTick = [1e3*temp 1e4*temp 1e5*temp 1e6];
XTickLabel = repmat({''},length(XTick),1);
XTickLabel{1}   = '$1\mathrm{kHz}$';
XTickLabel{10}  = '$10\mathrm{kHz}$';
XTickLabel{19}  = '$100\mathrm{kHz}$';
XTickLabel{end} = '$1\mathrm{MHz}$';

refAmpl = abs(simres.MS_node(1,1,2));
figure(5163543)
clf
subplot(121)
hold on
plot(log10(U.freq),db(U.mean/refAmpl),'Color',[0 0.7 0],'linewidth',2)
fill([log10(U.freq) fliplr(log10(U.freq))],db([U.mean+3*U.stdNL fliplr(U.mean-3*U.stdNL)]/refAmpl),...
    [0 0.7 0],'EdgeColor','none','FaceAlpha',0.5);
set(gca,'XTick',log10(XTick),'XTickLabel',XTickLabel);
grid on
legend({'$G_{R\rightarrow U}\BLA$','uncertainty'},'interpreter','none','Location','NW')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')

subplot(122)
hold on
plot(log10(Y.freq),db(Y.mean/abs(simres.MS_node(1,1,2))),'Color',[0 0.7 0],'linewidth',2)
fill([log10(Y.freq) fliplr(log10(Y.freq))],db([Y.mean+3*Y.stdNL fliplr(Y.mean-3*Y.stdNL)]/refAmpl),...
    [0 0.7 0],'EdgeColor','none','FaceAlpha',0.5);
set(gca,'XTick',log10(XTick),'XTickLabel',XTickLabel);
legend({'$G_{R\rightarrow Y}\BLA$','uncertainty'},'interpreter','none','Location','SW')
grid on
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')

cleanfigure
matlab2tikz('MILLER_ZBLA.tex','height','\figureheight','width','\figurewidth','parseStrings',false);


%%

figure(6844321)
clf
% errorbar_mimo(G.freq,G.mean,3*G.stdNL,'k',@db)
plot(log10(G.freq),db(G.mean),'-','Color',[0 0.7 0],'linewidth',2)
hold on
fill([log10(G.freq) fliplr(log10(G.freq))],db([G.mean+3*G.stdNL fliplr(G.mean-3*G.stdNL)]),...
    [0 0.7 0],'EdgeColor','none','FaceAlpha',0.5);

grid on
% ylim([-40 80])
set(gca,'XTick',log10(XTick),'XTickLabel',XTickLabel);
legend({'$G_{U\rightarrow Y}\BLA$','uncertainty'},'interpreter','none','Location','SW')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')

cleanfigure
matlab2tikz('MILLER_BLA.tex','height','\figureheight','width','\figurewidth','parseStrings',false);


