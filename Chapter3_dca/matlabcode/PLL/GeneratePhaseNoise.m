function noise = GeneratePhaseNoise(Nfft, Fs, psd_f_Hz, psd_val_dBc_Hz, PLOTTING,odd)
%
% function noise = GeneratePhaseNoise(Nfft, Fs, psd_f_Hz, psd_val_dBc_Hz, PLOTTING)
%
% Phase Noise Model generation
% 
%  INPUT:
%     Nfft - number of samples used for DFT
%     Fs  - sampling frequency [Hz]
%     psd_f_Hz  - frequencies at which SSB Phase Noise is defined (offset from carrier in Hz)
%     psd_val_dBc_Hz - SSB Phase Noise power [dBc/Hz]
%     PLOTTING  - 1: perform plotting, 0: don't perform plotting
%
%  OUTPUT:
%     noise - time domain representation of phase noise signal
%
%  EXAMPLE (How to use PhaseNoise) :
%         Define SSB phase noise as follows:
%      -------------------------------------------------------
%      |  Offset From Carrier      |        Phase Noise      |
%      -------------------------------------------------------
%      |        1   kHz            |        -84  dBc/Hz      |
%      |        10  kHz            |        -100 dBc/Hz      |
%      |        100 kHz            |        -96  dBc/Hz      |
%      |        1   MHz            |        -109 dBc/Hz      |
%      |        10  MHz            |        -122 dBc/Hz      |
%      -------------------------------------------------------
% 

if ~exist('odd','var')
    odd = true;
end

% Define variables
freq_v_Hz = (1:Nfft/2-1)*Fs/Nfft;     % Frequency vector [Hz]
delta_f   = Fs/Nfft;                % Frequency spacing [Hz]
F = Nfft/2-1;                       % Total number of harmonics
if odd
    lines = 2:2:F+1;                      % Excited lines in SSB
else
    lines = 2:1:F+1;                      % Excited lines in SSB
end
SSB = zeros(1, Nfft);               % Vector used for FFT
phase = 2*pi*rand(1, F);            % Random phase vector

% Interpolate in the frequency domain
psd_ssb_dBc_Hz = interp1(log(psd_f_Hz+eps), psd_val_dBc_Hz, log(freq_v_Hz+eps), 'linear');

% Remove all the NaN from the interpolation
psd_ssb_dBc_Hz(isnan(psd_ssb_dBc_Hz)) = -inf;

% Convert power to amplitude
psd_ssb_Hz = 10.^(psd_ssb_dBc_Hz / 20);

% Scaling factors:
% - multiply with Nfft due to FFT normalization
% - multiply with 1/sqrt(2) to get RMS value
% - multiply with sqrt(f) to go from dBc/Hz to dBc
%
psd_ssb = Nfft*sqrt(delta_f)*psd_ssb_Hz;

% Construct FFT vector and convert to time domain
SSB(lines) = psd_ssb(lines-1).*exp(1j*phase(lines-1));
SSB(1) = 0;
noise = 2*real(ifft(SSB));

if PLOTTING 
   X = fft(noise);
   SSB = X(2:F+1)/Nfft/sqrt(delta_f);
%     figure
%     semilogx(freq_v_Hz, psd_ssb_dBc_Hz, 'rx-')
%     title('Wanted response')
%     xlabel('Frequency offset')
%     ylabel('PSD [dBc/Hz]')

    figure
    subplot(211)
    semilogx(freq_v_Hz, db(SSB), 'x')
    title('Constructed response')
    xlabel('Frequency offset')
    ylabel('PSD[dBc/Hz]')
    subplot(212)
    semilogx(freq_v_Hz, angle(SSB), 'x')
    title('Constructed response')
    xlabel('Frequency offset')
    ylabel('PSD[dBc/Hz]')
    
    figure
    plot((0:Nfft-1)/Fs, 1/(2*pi*Fs)*noise)
    title('Time domain')
    xlabel('Time [s]')
    ylabel('Amplitude')
end

end
