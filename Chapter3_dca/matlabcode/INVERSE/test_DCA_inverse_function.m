% in this script, I try the DCA on a cascade of two blocks. The total
% circuit is linear, but it consists of two non-linear blocks that cancel
% out
clear all
close all
clc

stage1 = @exp;
stage2 = @log;

f0 = 1;
fmax = 100*f0;

M = 10000;

% generate a multisine
MSdef = MScreate(f0,fmax,'rms',0.5);

% generate many different phase relations of the multisine
for mm=1:M
    MSdef.phase = 2*pi*rand(size(MSdef.phase));
    [tmp,time] = MScalculatePeriod(MSdef,20);
    if mm==1
        ref = zeros(M,1,length(tmp));
    end
    ref(mm,1,:)=tmp;
end
  
% calculate the spectrum of the reference signal
spec.REF = fft(ref,[],3)/size(ref,3);
spec.REF = spec.REF(:,1,1:end/2);
% generate the frequency axis for the plots
spec.freq = (0:(size(spec.REF,3)-1))*f0;

% put the multisine through the first system
int = stage1(ref);
spec.INT = fft(int,[],3)/size(int,3);
spec.INT = spec.INT(:,1,1:end/2);

% and through the second system
out = stage2(int);
spec.OUT = fft(out,[],3)/size(out,3);
spec.OUT = spec.OUT(:,1,1:end/2);


figure(1)
clf
% plot the time-domain waveforms
subplot(231)
plot(time,squeeze(ref(1:10,1,:)));
subplot(232)
plot(time,squeeze(int(1:10,1,:)));
subplot(233)
plot(time,squeeze(out(1:10,1,:)));

% plot the stuff in the frequency domain as well
subplot(234)
plot(spec.freq,db(squeeze(spec.REF(1:10,1,:))));
xlim([0 3*fmax]);
subplot(235);
plot(spec.freq,db(squeeze(spec.INT(1:10,1,:))));
xlim([0 3*fmax]);
subplot(236);
plot(spec.freq,db(squeeze(spec.OUT(1:10,1,:))));
xlim([0 3*fmax]);

%% calculate the BLA of the two stages


[G,Y,U,exBins,CovYs] = calculateSISO_BLA(spec,'reference','REF','input',{'REF','INT'},'output',{'INT','OUT'});
nexBins = setdiff(1:length(spec.freq),exBins);


%%
BLAfreq = G(1).freq;

% plot the results of the BLA estimation
figure(2)
clf
subplot(121)
hold on
plot(BLAfreq,abs(G(1).mean),'Linewidth',2,'Color',[0 0.7 0]);
fill([BLAfreq fliplr(BLAfreq)],[abs(G(1).mean+3*G(1).stdNL) fliplr(abs(G(1).mean-3*G(1).stdNL))],[0 0.7 0],'EdgeAlpha',0,'FaceAlpha',0.5)

plot(BLAfreq,abs(G(2).mean),'Linewidth',2,'Color',[1 0.7 0]);
fill([BLAfreq fliplr(BLAfreq)],[abs(G(2).mean+3*G(2).stdNL) fliplr(abs(G(2).mean-3*G(2).stdNL))],[1 0.7 0],'EdgeAlpha',0,'FaceAlpha',0.5)
legend({'$G\BLA_{NL_1}$','uncertainty','$G\BLA_{NL_2}$','uncertainty'},'interpreter','none','location','E')

xlim([0 fmax])
ylim([0.8 1.2])
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none');
ylabel('Norm $\left[ \norm{\cdot} \right]$','interpreter','none');
grid on



subplot(122)
hold all
plot(BLAfreq,squeeze(real(CovYs(1,1,:))),'-','Color',[0 0.7 0],'Linewidth',2);
plot(BLAfreq,squeeze(real(CovYs(2,2,:))),'-','Color',[1 0.7 0],'Linewidth',2);
plot(BLAfreq,squeeze(real(CovYs(1,2,:))),'-','Color',[0 0.7 1],'Linewidth',2);
plot(BLAfreq,squeeze(imag(CovYs(1,2,:))),'--','Color',[0 0.7 1],'Linewidth',2);
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none');
grid on

legend({'$\left[ \mathbf{C}_{\Dvec} \right]_{1,1}$',...
    '$\left[ \mathbf{C}_{\Dvec} \right]_{2,2}$',...
    '$\Re\left\{ \left[ \mathbf{C}_{\Dvec} \right]_{1,2} \right\}$',...
    '$\Im\left\{ \left[ \mathbf{C}_{\Dvec} \right]_{1,2} \right\}$'},...
    'Interpreter','none','Location','E')


cleanfigure
matlab2tikz('INVERSE_BLAs.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

%% plot the internal signal and its distortion level
figure(546)
clf
% subplot(121)
% hold on
% plot(spec.freq( exBins),db(squeeze(spec.REF(1:10,:, exBins))),'k.','MarkerSize',1);
% % plot(spec.freq(nexBins(2:end)),db(squeeze(spec.INT(1:10,:,nexBins(2:end)))),'m.','MarkerSize',1);
% plot(BLAfreq,db(U(1).mean),'k-','Linewidth',2);
% % plot(spec.freq(2:end),db(Y(1).disto(2:end)),'m-','Linewidth',2);
% xlim([0 5*fmax]);
% ylim([-120 -20]);
% xlabel('Frequency $\left[ \mathrm{Hz} \right]','interpreter','none');
% subplot(122)
hold on
plot(spec.freq( exBins),db(squeeze(spec.INT(1:10,:, exBins))),'k.','MarkerSize',1);
plot(spec.freq(nexBins(2:end)),db(squeeze(spec.INT(1:10,:,nexBins(2:end)))),'m.','MarkerSize',1);
plot(BLAfreq,db(Y(1).mean),'k-','Linewidth',2);
plot(spec.freq(2:end),db(Y(1).disto(2:end)),'m-','Linewidth',2);
xlim([0 5*fmax]);
ylim([-120 -20]);
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none');
ylabel('Amplitude $\left[ \mathrm{dB} \right]$','interpreter','none');

cleanfigure
matlab2tikz('INVERSE_spectra.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

%%
% calculate the distortion contributions
T1 = G(2).mean;
T2 = ones(size(G(2).mean));

ContNL1 =    real(T1.*conj(T1).*squeeze(CovYs(1,1,:)).');
ContNL2 =    real(T2.*conj(T2).*squeeze(CovYs(2,2,:)).');
ContCov =  -2*real(T1.*conj(T2).*squeeze(CovYs(1,2,:)).');

figure(3)
clf
plot(BLAfreq,ContNL1,'+-','Color',[0 0.7 0],'Linewidth',2);
hold on
plot(BLAfreq,ContNL2,'-','Color',[1 0.7 0],'Linewidth',2);
plot(BLAfreq,ContCov,'Color',[0 0.7 1],'Linewidth',2);
plot(BLAfreq,ContNL1+ContNL2+ContCov,'Color',[0.7 0.7 0.7],'Linewidth',2);
legend({'NL1','NL2','Cov','sum'},'Location','W')
grid on
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none');
ylabel('Distortion contribution')

cleanfigure
matlab2tikz('INVERSE_contribs.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

