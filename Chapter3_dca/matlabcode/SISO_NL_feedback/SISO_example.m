clear all
close all
clc

% create the multisines
MSdef = MScreate(10e2,10e5,'grid','full','rms',0.1);
MSdef.MSnode = 'in';

% simulate the multisines
SPEC = ADSsimulateMS('SISO_example.net',MSdef,'Simulator','HB','numberOfRealisations',100);

% plot the spectra
figure(1)
clf
subplot(131);
plot(SPEC.freq,db(squeeze(SPEC.amp)),'+');
subplot(132);
plot(SPEC.freq,db(squeeze(SPEC.int)),'+');
subplot(133);
plot(SPEC.freq,db(squeeze(SPEC.out)),'+');

AC = ADSsimulateAC('SISO_example.net',MSdef);

%% calculate the SIMO BLA from the reference to the input/output of the stages

% calculate the BLAs of the different stages
[BLAs,Y,U,exBins,CovYs] = calculateSISO_BLA(SPEC,'input',{'amp','int'},'output',{'int','out'},'reference','in');


M = size(SPEC.out,1);
freq = BLAs(1).freq;
F = length(freq);

% plot the BLAs and their uncertainty
figure(2)
clf
subplot(121)
semilogx(freq,db(BLAs(1).mean),'Color',[0 0.7 0],'Linewidth',1);
hold on
semilogx(freq,db(BLAs(1).stdNL),'Color',[0 0.7 0]);
% title('input stage')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none');
grid on
legend({'$G_{U\stageNr[2] \rightarrow Y\stageNr[2]}^\mathrm{BLA}$','std'},'interpreter','none')

subplot(122)
semilogx(freq,db(BLAs(2).mean),'Color',[0 0.7 0],'Linewidth',1);
hold on
semilogx(freq,db(BLAs(2).stdNL),'Color',[0 0.7 0]);
% title('output stage')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none');
grid on
legend({'$G_{U\stageNr[2] \rightarrow Y\stageNr[2]}^\mathrm{BLA}$','std'},'interpreter','none')


cleanfigure
matlab2tikz('SISO_NL_BLA.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%% calculate the distortion contributions

% FB is the feedback factor
FB = 1/11;
B = [0 1];
FEEDBACK = [0 FB;-1 0];
% calculate Tout
Tout = zeros(2,F);
for ff=1:F
    Tout(:,ff) = B/(eye(2)+diag([BLAs(1).mean(ff),BLAs(2).mean(ff)])*FEEDBACK);
end
CONTin  =        Tout(1,:).*conj(Tout(1,:)).*squeeze(CovYs(1,1,:)).';
CONTout =        Tout(2,:).*conj(Tout(2,:)).*squeeze(CovYs(2,2,:)).';
CONTcov = 2*real(Tout(2,:).*conj(Tout(1,:)).*squeeze(CovYs(1,2,:)).');

% fun = @(x) db(x)/2;
fun = @(x) x;

figure(3)
clf
% plot(SPEC.freq(exBins),db(DISTO),'.','MarkerSize',1,'Color',[0.5 0.5 0.5]);
hold on
plot(freq/1e6,CONTout,'Color',[0 0.7 0]);
plot(freq/1e6,CONTin ,'Color',[1 0.7 0]);
plot(freq/1e6,CONTcov,'Color',[0 0.7 1]);
% leg = legend('$\sigma_{y_2}$','$\sigma_{y_2}$','$\sigma_{y_1 y_2}$');
% set(leg,'Location','SouthEast','Interpreter','none');
plot(SPEC.freq/1e6,Y(2).disto.^2,'Color',[0.7 0.7 0.7],'LineWidth',1);
% hold on
% plot(freq/1e6,CONTout+CONTin+CONTcov,'k-');
xlim([0 1])
% ylim([-100 -70])
% set(gca,'XTick',[0 0.5 1]);
% set(gca,'YTick',[-100 -80]);
grid on
legend({'output','input','covariance','$\mathbb{E}\left\{\DistoSISO \DistoSISO^\herm \right\}$'},'interpreter','none')
% text(0.8,-75,'$\sigma_{y_2}$','Interpreter','none','VerticalAlignment','bottom','Color',[1 0.7 0],'BackgroundColor',[1 1 1]);
% text(0.5,-85,'$\sigma_{y_1}$','Interpreter','none','VerticalAlignment','bottom','Color',[0 0.7 1],'BackgroundColor',[1 1 1]);
% text(0.2,-95,'$\sigma_{y_1 y_2}$','Interpreter','none','VerticalAlignment','bottom','Color',[0.7 0.7 0.7],'BackgroundColor',[1 1 1]);
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Distortion contribution $\left[ \mathrm{V^2} \right]$','interpreter','none');


cleanfigure
matlab2tikz('SISO_NL_contributions.tex','height','\figureheight','width','\figurewidth','parseStrings',false)


% save 100real SPEC

%% 

% calculate the distortion at the output
DISTO = zeros(M,length(exBins));
for mm=1:M
    DISTO(mm,:) = squeeze(SPEC.out(mm,1,exBins)) - Y(2).mean.'.*squeeze(SPEC.in(mm,1,exBins))./squeeze(abs(SPEC.in(mm,1,exBins)));
end

figure(10)
clf
hold on
plot(SPEC.freq(exBins(1:5:end))/1e6,db(squeeze(SPEC.out(1:5,1,exBins(1:5:end)))),'k.','MarkerSize',1);

% plot(SPEC.freq(exBins)/1e6,db(squeeze(SPEC.out(1:5,1,exBins))),'.','Color',[0.5 0.5 0.5],'MarkerSize',1);
plot(SPEC.freq(max(exBins)+1:end)/1e6,db(squeeze(SPEC.out(1:5,1,max(exBins)+1:end))),'.','Color',[1 0 1],'MarkerSize',1);
% plot(SPEC.freq(exBins(1:5:end))/1e6,db(DISTO(1:5,1:5:end)),'.','Color',[0.5 0.5 0.5],'MarkerSize',1);
plot(SPEC.freq(1:5:end)/1e6,db(Y(2).disto(1:5:end)),'Linewidth',2,'color',[1 0 1]);

xlim([0 1.5]);
ylim([-100 -20])
% set(gca,'XTick',[0 1]);
set(gca,'YTick',[-100 -80 -60 -40 -20]);
grid on
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Output $\left[ \mathrm{dBV} \right]$','interpreter','none');

cleanfigure
matlab2tikz('SISO_NL_output.tex','height','\figureheight','width','\figurewidth','parseStrings',false)

