function errorbardb(freq,frf,std,varargin)


% calculate the lower and upper error bar
upper = abs(frf)+std;
lower = abs(frf)-std;
% when the error is bigger than the value itself, make the lower bound equal to eps
lower(abs(frf)<std)=eps;
% plot the errorbar thing
errorbar(freq,db(frf),db(lower)-db(frf),db(upper)-db(frf),varargin{:});

end