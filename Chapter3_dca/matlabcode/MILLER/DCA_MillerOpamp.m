clear all
close all
clc

Netlist = readTextFile('MillerOpamp.net');

MSdef = MScreate(1e5,10e6,'grid','random-odd','rms',0.1);
MSdef.MSnode = {'MS_node','sgnd'};

% spec=ADSsimulateMS(Netlist,MSdef,'Simulator','HB','numberOfRealisations',10);
% plotMSresponse(spec.freq,spec.out,MSdef,'figNum',999);

%%

% RESbal = DCA('MillerOpamp_zoom.net','MSdef',MSdef,'BLAtechnique','Sparam','numberOfRealisations',50,'unbal2bal',true,...
%     'loadOldResults',true,'save',false,'cleanup',true,'showsteps',true,'display',false);

% [BLAs,waves,spec,specraw,wavesraw] = DCA_estimate_BLAs(Netlist,MSdef,'technique','ZIPPER',...
%     'numberOfRealisations',20,'TicklerAmplitudes',1e-6,'OneByOne',true);
% save('BLAs.mat','BLAs','waves','spec');

%%
% 
% figure(1)
% clf
% for ii=1:2
%     for jj=1:2
%         subplot(2,2,2*(ii-1)+jj)
%         hold on
%         errorbar_mimo(BLAs.freq,BLAs.TOTAL.G(ii,jj,:),sqrt(BLAs.TOTAL.varG(ii,jj,:)),[],@db);
%         plot(BLAs.freq_lin,db(squeeze(BLAs.TOTAL.G_linear(ii,jj,:))));
%         title([num2str(ii) num2str(jj)])
%     end
% end
% % errorbar_mimo(BLAs.freq,BLAs.TOTAL.G,sqrt(BLAs.TOTAL.varG),[],@db);
% % plot_mimo(BLAs.freq_lin,db(BLAs.TOTAL.G_linear));
% % title('TOTAL')
% 
% figure(2)
% clf
% for ii=1:2
%     for jj=1:2
%         subplot(2,2,2*(ii-1)+jj)
%         hold on
%         errorbar_mimo(BLAs.freq,BLAs.IN.G(ii,jj,:),sqrt(BLAs.IN.varG(ii,jj,:)),[],@db);
%         plot(BLAs.freq_lin,db(squeeze(BLAs.IN.G_linear(ii,jj,:))));
%         title([num2str(ii) num2str(jj)])
%     end
% end
% % errorbar_mimo(BLAs.freq,BLAs.IN.G,sqrt(BLAs.IN.varG),[],@db);
% % plot_mimo(BLAs.freq_lin,db(BLAs.IN.G_linear));
% % title('IN')
% 
% figure(3)
% clf
% for ii=1:2
%     for jj=1:2
%         subplot(2,2,2*(ii-1)+jj)
%         hold on
%         errorbar_mimo(BLAs.freq,BLAs.OUT.G(ii,jj,:),sqrt(BLAs.OUT.varG(ii,jj,:)),[],@db);
%         plot(BLAs.freq_lin,db(squeeze(BLAs.OUT.G_linear(ii,jj,:))));
%         title([num2str(ii) num2str(jj)])
%     end
% end
% errorbar_mimo(BLAs.freq,BLAs.OUT.G,sqrt(BLAs.OUT.varG),[],@db);
% plot_mimo(BLAs.freq_lin,db(BLAs.OUT.G_linear));
% title('OUT')




%%
setpref('ADS','parallelSims',3);
RES = DCA(Netlist,'MSdef',MSdef,'loadOldResults',false,'BLAtechnique','SPARAM','display',false,'numberOfRealisations',100);
setpref('ADS','parallelSims',1);


%% plot the output wave of the circuit

% calculate the distortion in the output wave
[G,Y,~,exBins] = calculateSISO_BLA(RES.waves,'input','ref','reference','ref','output','W_TOTAL_p2_B');
nexbins = setdiff(2:2:length(RES.waves.freq),exBins);

Mtoplot = 1:6;

figure(501)
clf
hold on
plot(RES.waves.freq(exBins) /1e6,squeeze(db(RES.waves.W_TOTAL_p2_B(Mtoplot,end,exBins ))),'k.','markersize',1);
plot(RES.waves.freq(1:2:end)/1e6,squeeze(db(RES.waves.W_TOTAL_p2_B(Mtoplot,end,1:2:end))),'b.','markersize',1);
plot(RES.waves.freq(nexbins)/1e6,squeeze(db(RES.waves.W_TOTAL_p2_B(Mtoplot,end,nexbins))),'r.','markersize',1);
plot(RES.waves.freq(2:2:end)/1e6,db(Y.disto(2:2:end)),'r-','Linewidth',2);
plot(RES.waves.freq(3:2:end)/1e6,db(Y.disto(3:2:end)),'b-','Linewidth',2);

xlim([0 25]);
ylim([-140 -40]);
set(gca,'YTick',[-120 -80 -40]);
set(gca,'XTick',[0 10 20]);
grid on
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
text(10,-40,'Output wave','interpreter','none','VerticalAlignment','top');

cleanfigure
matlab2tikz('MILLER_disto.tex','height','\figureheight','width','\figurewidth','parseStrings',false)

%% plot the Z-parameters of the different stages

RES.BLAs.IN.Z = convertRepresentation(RES.BLAs.IN.G,'S','Z',50);
RES.BLAs.OUT.Z = convertRepresentation(RES.BLAs.OUT.G,'S','Z',50);


figure(62962)
clf
subplot(121)
hold on
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.IN.Z(1,1,:))),'-','Color',[0 0.7 0],'Linewidth',2);
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.IN.Z(2,1,:))),'--','Color',[0 0.7 0],'Linewidth',2);
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.IN.Z(1,2,:))),'-.','Color',[0 0.7 0],'Linewidth',2);
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.IN.Z(2,2,:))),':','Color',[0 0.7 0],'Linewidth',2);
title('IN')
legend({'$Z_{11}$','$Z_{21}$','$Z_{12}$','$Z_{22}$'})
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none');
grid on

subplot(122)
hold on
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.OUT.Z(1,1,:))),'-','Color',[1 0.7 0],'Linewidth',2);
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.OUT.Z(2,1,:))),'--','Color',[1 0.7 0],'Linewidth',2);
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.OUT.Z(1,2,:))),'-.','Color',[1 0.7 0],'Linewidth',2);
plot(RES.BLAs.freq/1e6,db(squeeze(RES.BLAs.OUT.Z(2,2,:))),':','Color',[1 0.7 0],'Linewidth',2);
legend({'$Z_{11}$','$Z_{21}$','$Z_{12}$','$Z_{22}$'})
title('OUT')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none');
grid on

cleanfigure
matlab2tikz('MILLER_BLAs.tex','height','\figureheight','width','\figurewidth','parseStrings',false)




%% plot the FRF from each distortion source to the output

cols(1,:) = [0 0.7 0];
cols(2,:) = [0 0.7 0];
cols(3,:) = [1 0.7 0];
cols(4,:) = [1 0.7 0];
marks = {'-','--','-','--'};

figure(118134)
clf
subplot(121)
hold on
for pp=1:4
    plot(RES.BLAs.freq/1e6,squeeze(db(RES.BLAs.Tout(2,pp,:))),marks{pp},'Color',cols(pp,:),'linewidth',2);
end
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none');
legend(cellfun(@(x) ['$' x '$'],RES.CONTs.PortNames,'UniformOutput',false),'interpreter','none');
grid on

subplot(122)
hold on
for pp=1:4
    plot(RES.BLAs.freq/1e6,squeeze(angle(RES.BLAs.Tout(2,pp,:))),marks{pp},'Color',cols(pp,:),'linewidth',2);
end
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Phase $\left[ \mathrm{rad} \right]$','interpreter','none');
legend(cellfun(@(x) ['$' x '$'],RES.CONTs.PortNames,'UniformOutput',false),'interpreter','none');
grid on

cleanfigure
matlab2tikz('MILLER_Tout.tex','height','\figureheight','width','\figurewidth','parseStrings',false)

%% plot the obtained distortion contributions

freq = RES.BLAs.freq;
ContAll = RES.CONTs.TOTAL_p2;

ContIN  = squeeze(ContAll(1,1,:) + ContAll(2,2,:) + ContAll(2,1,:));
ContOUT = squeeze(ContAll(3,3,:) + ContAll(4,4,:) + ContAll(4,3,:));
ContCOV = 0.5*squeeze(ContAll(3,1,:) + ContAll(4,1,:) + ContAll(3,2,:) + ContAll(3,2,:)); % TODO where does this factor 2 come from?
ContSUM = ContCOV + ContOUT + ContIN;
DISTO = real(squeeze(RES.BLAs.TOTAL.Cd_conn(2,2,:)));

figure(812456)
clf
hold on
title('Odd frequency bins')
plot(freq(1:2:end)/1e6,ContIN(1:2:end),'Color',[0 0.7 0],'Linewidth',2);
plot(freq(1:2:end)/1e6,ContOUT(1:2:end),'Color',[1 0.7 0],'Linewidth',2);
plot(freq(1:2:end)/1e6,ContCOV(1:2:end),'Color',[0 0.7 1],'Linewidth',2);
% plot(freq(1:2:end),ContSUM(1:2:end),'Color',[0 0 0],'Linewidth',1);
plot(freq(1:2:end)/1e6,DISTO(1:2:end),'Color',[0.5 0.5 0.5],'Linewidth',2);
legend({'IN','OUT','COV','SUM','DISTO'})
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
grid on

% cleanfigure
matlab2tikz('MILLER_contribs_odd.tex','height','\figureheight','width','\figurewidth','parseStrings',false)


figure(216359680)
clf
hold on
title('Even frequency bins')
plot(freq(2:2:end)/1e6,ContIN(2:2:end),'Color',[0 0.7 0],'Linewidth',2);
plot(freq(2:2:end)/1e6,ContOUT(2:2:end),'Color',[1 0.7 0],'Linewidth',2);
plot(freq(2:2:end)/1e6,ContCOV(2:2:end),'Color',[0 0.7 1],'Linewidth',2);
% plot(freq(2:2:end),ContSUM(2:2:end),'Color',[0 0 0],'Linewidth',1);
plot(freq(2:2:end)/1e6,DISTO(2:2:end),'Color',[0.5 0.5 0.5],'Linewidth',2);
legend({'IN','OUT','COV','SUM','DISTO'})
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
grid on

% cleanfigure
matlab2tikz('MILLER_contribs_even.tex','height','\figureheight','width','\figurewidth','parseStrings',false)



%% use yet another method to determine whether the BLA is decent or not ...

% 
figure(555)
clf
hold on
plot(RES.SIMO.Measured.freq/1e6 , db(RES.SIMO.Measured.W_OUT_p2_B),'+-' ,'Color',[0 0.7 0],'Linewidth',2);
plot(RES.SIMO.Predicted.freq/1e6 , db(RES.SIMO.Predicted.W_OUT_p2_B),'-' ,'Color',[1 0.7 0],'Linewidth',2);
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none');
legend({'Measured $\mathbf{G}_{R\rightarrow OUT }$','Predicted $\mathbf{G}_{R\rightarrow OUT }$'},'interpreter','none','location','NE');
grid on

cleanfigure
matlab2tikz('MILLER_BLAtest.tex','height','\figureheight','width','\figurewidth','parseStrings',false)


%%

% 
% signals = fieldnames(RES.SIMO.Measured);
% % remove the freq axis from the thing
% signals = signals(cellfun(@(x) isempty(x),regexp(signals,'freq')));
% 
% colors = hsv(length(signals));
% for ss=1:length(signals)
%     plot(RES.SIMO.Measured.freq , db(RES.SIMO.Measured.(signals{ss})) ,'Color',colors(ss,:));
% end
% legend(signals,'interpreter','none')
% % plot the predicted responses
% for ss=1:length(signals)
%     plot(RES.SIMO.Predicted.freq , db(RES.SIMO.Predicted.(signals{ss})) ,'--','Color',colors(ss,:));
% end

