function plotMSresponse(varargin)

p = inputParser();

% frequency axis of the simulation
p.addRequired('freq',@isvector);

% matrix that contains the response that should be plotted.
% This matrix should be a MxPxF matrix
p.addRequired('Resp',@isnumeric);

% MSdef is the struct that describes the multisine that was connected to
% the circuit. Create it with the MScreate function
p.addRequired('MSdef',@isstruct);

% number of the figure in which the spectrum will be plotted
p.addParameter('figNum',1,@isscalar);

% function that should be applied to the data
p.addParameter('plotFunction',@db,@(x) isa(x,'function_handle'));

p.parse(varargin{:});
args = p.Results;
clear p


% calculate the k numbers of the frequency axis
binNums = round(args.freq/args.freq(2));


% the excited bins are stored in MSdef.grid
binGroups(1).Name = 'excited bins';
binGroups(1).Color = [0 0 0];
[~,ind] = intersect(binNums,args.MSdef.grid);
binGroups(1).Indexes = ind;

% if the multisine is odd, we can split the even and odd non-linear
% distortion and give it their separate color
switch args.MSdef.info
    case {'odd','random-odd'}
        % get the even bins from the list
        binGroups(2).Name = 'even bins';
        binGroups(2).Color = [0 0 1];
        binGroups(2).Indexes = find(~mod(binNums,2));
        % get the unexcited odd bins from the list
        binGroups(3).Name = 'odd bins';
        binGroups(3).Color = [1 0 0];
        binGroups(3).Indexes = setdiff(find(mod(binNums,2)),binGroups(1).Indexes);
    otherwise
        % just get the unexcited frequency bins
        binGroups(2).Name = 'unexcited bins';
        binGroups(2).Color = [0.5 0.5 0.5];
        binGroups(2).Indexes = setdiff(binNums,binGroups(1).Indexes);
end

% now plot all the groups
figure(args.figNum)
hold on

for pp=1:length(binGroups)
    % first vectorise the data
    [freq_vec,data_vec] = vectorize(args.freq,args.plotFunction(args.Resp),binGroups(pp).Indexes);
    % and plot
    plot(freq_vec,data_vec,'+','Color',binGroups(pp).Color);
end
hold off

end

function [freq_vec,data_vec] = vectorize(freq,data,bins)
    % this function ensures that only a single vector of data is plotted,
    % to allow better processing by cleanfigure afterwards
    [M,~,F] = size(data);
    freq_vec = zeros([M 1 F]);
    for mm=1:M
        freq_vec(mm,1,:) = freq;
    end
    % vectorise the data and the frequency axis
    data_vec = vec(data(:,end,bins));
    freq_vec = vec(freq_vec(:,1,bins));
end