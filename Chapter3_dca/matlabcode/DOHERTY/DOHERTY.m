clear all
close all
clc

% build the main multisine
MSdef(1) = MScreate(0.125e6,2140e+6,'mode','bandpass','grid','even','numtones',81,'rms',3,'Rout',0);
% also design the tickler excitation
MSdef(2) = MScreate(0.125e6,2140e6,'mode','bandpass','grid','odd','numtones',85,'Rout',Inf,'rms',1e-6);

MSdef(1).MSnode = {'ref' ,'0'};
MSdef(2).MSnode = {'ref2','0'};

% add a current source to measure the second reference multisine
Netlist = 'DOHERTY.net';
Netlist = readTextFile(Netlist);
Netlist{end+1} = ADSaddShort(-1,'tickleref','sink','ref2','source','Vout');

HBsettings.Freq = [0.125e6 2140e6];
HBsettings.Order = [400 10];
HBsettings.MaxOrder = 400;

setpref('ADS','parallelSims',1);
[waves,spec] = ADSsimulateMSwaves(Netlist,MSdef,'simulator','HB','numberOfRealisations',50,'SimSettingsStruct',HBsettings);

% save dohertysims_10MHz_holes waves spec

% AC = ADSsimulateAC(Netlist,MSdef);

figure(1)
plot(spec.freq,db(squeeze(spec.V_main_p2)),'+');
% xlim([2e9 2.3e9]);



%% calculate the BLAs of the different stages

% do a linear analysis as well. Add a voltage source to replace the
% multisine and call the Linear Analysis function
Netlist = 'DOHERTY.net';
Netlist = readTextFile(Netlist);
Netlist{end+1} = ADSaddV_Source(-1,'MS','p','ref','n','0','Vdc',0);
BLAs = Linear_Analysis_v2(Netlist,'fstart',MSdef(1).fmin,'fstop',MSdef(1).fmax,'fstep',MSdef(1).f0);
% also determine the S-parameters of the total circuit and of the load impedances seen by the circuit
TOTAL = Linear_Analysis_v2(Netlist,'fstart',MSdef(1).fmin,'fstop',MSdef(1).fmax,'fstep',MSdef(1).f0,'onlyTotal',true,'getLoadImps',true);
BLAs.TOTAL = TOTAL.TOTAL;
BLAs.LOAD = TOTAL.LOAD;


reference{1} = 'ref1';
reference{2} = 'ref2';

waves.ref1 = spec.ref;
waves.ref2 = spec.I_tickleref;

% start with the total amplifier
inputs{1}  = 'W_TOTAL_p1_A';
inputs{2}  = 'W_TOTAL_p2_A';
outputs{1} = 'W_TOTAL_p1_B';
outputs{2} = 'W_TOTAL_p2_B';
[TOTAL] = extractMIMO_BLA_zippered(waves,...
    'input',inputs,...
    'output',outputs,...
    'reference',reference,'plot',false);

% now the main transistor
inputs{1}  = 'W_main_p1_A';
inputs{2}  = 'W_main_p2_A';
outputs{1} = 'W_main_p1_B';
outputs{2} = 'W_main_p2_B';
[MAIN] = extractMIMO_BLA_zippered(waves,...
    'input',inputs,...
    'output',outputs,...
    'reference',reference,'plot',false);

% and the peaking transistor
inputs{1}  = 'W_aux_p1_A';
inputs{2}  = 'W_aux_p2_A';
outputs{1} = 'W_aux_p1_B';
outputs{2} = 'W_aux_p2_B';
[AUX] = extractMIMO_BLA_zippered(waves,...
    'input',inputs,...
    'output',outputs,...
    'reference',reference,'plot',false);

% combine the results of the linear analysis and the BLAs
BLAs.freq_lin = BLAs.freq;
BLAs.freq = MAIN.freq;

BLAs.TOTAL.G_lin = BLAs.TOTAL.G;
BLAs.TOTAL.G = TOTAL.G;
BLAs.TOTAL.varG = TOTAL.varG;

BLAs.main.G_lin = BLAs.main.G;
BLAs.main.G = MAIN.G;
BLAs.main.varG = MAIN.varG;

BLAs.aux.G_lin = BLAs.aux.G;
BLAs.aux.G = AUX.G;
BLAs.aux.varG = AUX.varG;


% do some plottin'
figure(789)
clf
leg = errorbar_mimo(BLAs.freq,BLAs.TOTAL.G,sqrt(BLAs.TOTAL.varG),[],@db);
plot_mimo(BLAs.freq_lin,db(BLAs.TOTAL.G_lin),'.');
title('TOTAL');
legend(leg)
%%
xlims=[2.135 2.145];
figure(788)
clf
subplot(121)
leg = errorbar_mimo(BLAs.freq/1e9,BLAs.main.G,sqrt(BLAs.main.varG),[],@db);
plot_mimo(BLAs.freq_lin/1e9,db(BLAs.main.G_lin),'.');
title('MAIN');
xlim(xlims)
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')
grid on
legend(leg)
subplot(122)
leg = errorbar_mimo(BLAs.freq/1e9,BLAs.aux.G,sqrt(BLAs.aux.varG),[],@db);
plot_mimo(BLAs.freq_lin/1e9,db(BLAs.aux.G_lin),'.');
title('AUX');
legend(leg)
grid on
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')
xlim(xlims)

matlab2tikz('DOHERTY_BLAs.tex','height','\figureheight','width','\figurewidth','parseStrings',false)


%%
% determine the covariance matrix of distortion sources
fields = fieldnames(waves);
for ff=1:length(fields);
    if ~strcmpi(fields{ff},'freq');
        cleanwaves.(fields{ff}) = waves.(fields{ff})(:,1,1:2:end);
    else
        cleanwaves.freq = waves.freq(1:2:end);
    end
end
cleanwaves.ref = cleanwaves.ref1;
[BLAs,Cd] = Calculate_Distortion_v4(cleanwaves,BLAs,'S');

% and do the contribution analysis
[CONTs,BLAs] = ContributionAnalysis(BLAs,Cd);

CONTs.freq = MSdef(1).fmin:2*MSdef(1).f0:MSdef(1).fmax;

% check whether the sum of contributions matches the total disto @ out
% determine the BLA from reference to the output wave
[~,Yout,~,exBins] = calculateSISO_BLA(cleanwaves,'input','ref','reference','ref','output','W_TOTAL_p2_B');
nexBins = setdiff(1:length(cleanwaves.freq),exBins);



%% generate some plots

figure(808)
clf

% combine the contributions
auxCont  = squeeze(sum(sum(CONTs.TOTAL_p2(1:2,1:2,:),1),2));
mainCont = squeeze(sum(sum(CONTs.TOTAL_p2(3:4,3:4,:),1),2));
covCont  = squeeze(sum(sum(CONTs.TOTAL_p2(3:4,1:2,:),1),2));

% determine the sum of contributions
sumConts = squeeze(sum(sum(CONTs.TOTAL_p2,1),2));

hold on
plot(CONTs.freq/1e9,100*mainCont./sumConts,'b','Linewidth',2);
plot(CONTs.freq/1e9,100*auxCont ./sumConts,'r','Linewidth',2);
plot(CONTs.freq/1e9,100*covCont ./sumConts,'m','Linewidth',2);
legend('main','aux','cov','Location','East');
set(gca,'YTick',[25 50 75 100])
set(gca,'XTick',[2.135 2.14 2.145])
xlim([2.135 2.145]);
grid on
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none')
text(2.135,90,'Distortion Contribution $\left[ \% \right]$','interpreter','none')

matlab2tikz('DOHERTY_contribs.tex','height','\figureheight','width','\figurewidth','parseStrings',false)


%%

figure(101)
clf
plot(cleanwaves.freq(exBins)/1e9,db(squeeze(cleanwaves.W_TOTAL_p2_B(1:10,1,exBins)).'),'k.','markersize',1);
hold on
plot(cleanwaves.freq(nexBins)/1e9,db(squeeze(cleanwaves.W_TOTAL_p2_B(1:10,1,nexBins)).'),'r.','markersize',1);
plot(CONTs.freq/1e9,db(squeeze(sum(sum(CONTs.TOTAL_p2(:,:,:),1),2)))/2,'r*');
plot(cleanwaves.freq/1e9,db(Yout.disto),'m-','Linewidth',1);
xlim([2.10 2.18])
ylim([-80 0])
set(gca,'XTick',[2.11 2.14 2.17])
set(gca,'YTick',[-80 -40 0])
grid on
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none')
ylabel('Output wave $\left[ \mathrm{dB} \right]$')
cleanfigure
matlab2tikz('DOHERTY_spectra.tex','height','\figureheight','width','\figurewidth','parseStrings',false)


%% look into the holes to see whether the BLA is correct

QUAL = checkBLAquality(BLAs,cleanwaves);

figure
plot(CONTs.freq,db(QUAL.main.D),'r-')
hold on
plot(CONTs.freq,db(QUAL.aux.D),'b-')


