%% In this script, I generate plots to show the complexity of modern signals


%% two-tone signals

sig = zeros(10000,1);

sig(100)=0.5;
sig(110)=0.5;

y = real(ifft(sig))*length(sig)*0.75;
time = linspace(0,1,length(sig));

Y = fft(y)/length(y);
freq = 1:length(sig);

figure(1)
clf

subplot(131)
[counts,bins] = hist(y,40);
h=barh(bins,counts);
set(get(h,'Parent'),'xdir','r')
set(gca,'Ytick',[-1 -0.5 0 0.5 1],'YTickLabel','');
set(gca,'Xtick',[]);
title('Distribution','FontWeight','Normal')
xlabel('Occurrence')
ylim([-1 1])

subplot(132)
plot(time,y);
title('Time domain','FontWeight','Normal')
xlabel('Time [s]')
ylim([-1 1])

subplot(133)
plot(freq(1:end/2)/1e3,db(Y(1:end/2)));
xlabel('Frequency [kHz]')
xlim([0 1])
ylim([-200 0])
set(gca,'Xtick',[0.1 0.5 0.75 1])
title('Frequency domain [dB]','FontWeight','Normal')

cleanfigure
matlab2tikz('TwoTonesignal.tex','width','\figurewidth','height','\figureheight','floatformat','%8.6g')


%% low-frequency example
% extract from Rage Against The Machine's Wake up to demonstrate audio signals

% load the whole thing
[y,Fs] = audioread('07 Wake Up.mp3');

% cut out a part in the middle somewhere
y = y(end/2-10000:end/2+10000,1);

% generate the time vector
time = (0:length(y)-1)*(1/Fs);

% calculate the spectrum of the signal
Y = fft(y.*hann(length(y)))/length(y);

% generate the frequency axis
freq = (0:length(y)-1)/length(y)*Fs;

figure(2)
clf
subplot(131)

[counts,bins] = hist(y,40);
h=barh(bins,counts);
set(get(h,'Parent'),'xdir','r')
set(gca,'Ytick',[-1 -0.5 0 0.5 1],'YTickLabel','');
set(gca,'Xtick',[]);
title('Distribution','FontWeight','Normal')
xlabel('Occurrence')
% grid on

subplot(132)
plot(time(1:100:end),y(1:100:end))
xlabel('Time [s]')
% xlim([0 4])
ylim([-1 1])
set(gca,'YTick',[-1 -0.5 0 0.5 1])
title('Time domain','FontWeight','Normal');
% grid on

subplot(133)
plot(freq(1:100:round(end/2))/1e3,db(Y(1:100:round(end/2))))
xlabel('Frequency [kHz]');
% grid on
ylim([-160 -30])
xlim([0 20])
set(gca,'YTick',[-160 -120 -80 -40])
title('Frequency domain [dB]','FontWeight','Normal')

cleanfigure
matlab2tikz('Audiosignal.tex','width','\figurewidth','height','\figureheight','floatformat','%8.6g')

%% apply the LF signal to a non-linear system

% build a multisine that resembles the audio signal
Y = fft(y)/length(y);
MS = Y;
% cut at the high frequencies
MS(7350:end) = 0;
% remove the lowest frequencies too
MS(1:40)=0;
% omit some of the bins in the signal
MS(1:50:end)=0;

% go back to the time domain with ms
ms = 2*real(ifft(MS))*length(MS);

figure(101)
clf
plot(db(Y(1:floor(end/2))));%freq(1:floor(end/2)),
hold on
plot(db(MS+eps),'r.')
figure(123)
clf
plot(ms)

% soundsc(ms,Fs)

%%

% create the actual multisine source with this multisine
bins = find(MS~=0)-1;
ampl = sqrt(MS(bins+1).*conj(MS(bins+1)));
phas = angle(MS(bins+1));


f0 = round(1000*freq(2))/1000;
MSdef = MScreate(f0,max(bins)*f0);

MSdef.grid = bins;
MSdef.freq = MSdef.grid*f0;
MSdef.ampl = ampl;
MSdef.phase = phas;

[per,time] = MScalculatePeriod(MSdef,3);

figure(120)
subplot(211)
plot(time,per);
subplot(212)
plot(db(fft(per)))

soundsc(per,1/time(2))

MSdef.MSnode = 'ref';
try
    SPEC = ADSsimulateMS('lowFreqExample.net',MSdef,'phase',MSdef.phase,'simulator','TRAN','periods',2);
    save OPAMPresponse SPEC
catch
    load OPAMPresponse
end

% plot the response of the op-amp to the music
figure(108)
subplot(211)
plot(SPEC.freq,db(squeeze(SPEC.ref(1,end,:))),'+')
subplot(212)
plot(SPEC.freq,db(squeeze(SPEC.out(1,end,:))),'+')

%% high-frequency example
% In this high-frequency example, I show a Wireless LAN signal around 2.4GHz


figure(3)
clf

subplot(131)

[counts,bins] = hist(y,40);
h=barh(bins,counts);
set(get(h,'Parent'),'xdir','r')
set(gca,'Ytick',[-1 -0.5 0 0.5 1],'YTickLabel','');
set(gca,'Xtick',[]);
title('Distribution','FontWeight','Normal')
xlabel('Occurrence')

subplot(132)
plot(0)
xlabel('Time [s]')
xlim([0 4])
ylim([-1 1])
set(gca,'YTick',[-1 -0.5 0 0.5 1])
title('Time domain','FontWeight','Normal');


subplot(133)
plot(0)
xlabel('Frequency [kHz]');
ylim([-160 -30])
xlim([0 20])
set(gca,'YTick',[-160 -120 -80 -40])
title('Frequency domain [dB]','FontWeight','Normal')

cleanfigure
matlab2tikz('WLANsignal.tex','width','\figurewidth','height','\figureheight','floatformat','%8.6g')






