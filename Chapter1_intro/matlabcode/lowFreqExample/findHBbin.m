function res = findHBbin(bin,freq)
% this function looks for the correct bin in the HB frequency grid

% normalise freq
freq = freq/freq(2);

[~,res]=min(abs(bin-freq));

end