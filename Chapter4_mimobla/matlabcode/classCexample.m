clear variables
close all
clc

cd C:\users\adam.cooman\Google' Drive'\Contributies\CAS' paper'\matlabcode\ex4_classC\

% the main multisine source is placed around 1GHz
MSdef = MScreate(1e6,1e9,'mode','bandpass','numTones',41,'rms',0.2);
MSdef.MSnode = {'in','bias'};

TicklerDistance = 1;

[BLAs,spec,waves,specraw,wavesraw,MSdefs] = Zipper_Analysis('classCnetlist.net',MSdef,'StagesToEstimate',{'TRAN'},...
     'numberOfRealisations',4,'TicklerAmplitudes',1e-6,'TicklerDistance',TicklerDistance,'plot',true);
 

 
%%



% find the frequency bins that belong to the main multisine
MainBins = find(abs(wavesraw.freq - MSdef.f0*round(wavesraw.freq/MSdef.f0))<(0.5*TicklerDistance)); 
TickBins = setdiff(1:length(wavesraw.freq),MainBins);

% find the excited frequency bins from the main multisine
ExBins = zeros(size(MSdefs(1).grid));
for ff=1:length(MSdefs(1).grid)
    [~,ExBins(ff)] = min(abs(wavesraw.freq(MainBins)-MSdefs(1).f0*MSdefs(1).grid(ff)));
end
ExBins = MainBins(ExBins);

% find the excited frequency bins for the tickler
ExBinsTick = zeros(size(MSdefs(2).grid));
for ff=1:length(MSdefs(2).grid)
    [~,ExBinsTick(ff)] = min(abs(wavesraw.freq(TickBins)-MSdefs(2).f0*MSdefs(2).grid(ff)-TicklerDistance));
end
ExBinsTick = TickBins(ExBinsTick);

figure(16532)
clf
hold on
plot(wavesraw.freq(MainBins),db(squeeze(wavesraw.W_TRAN_p2_B(:,end,MainBins))),'r.');
plot(wavesraw.freq(ExBins),db(squeeze(wavesraw.W_TRAN_p2_B(:,end,ExBins))),'k+');
plot(wavesraw.freq(TickBins),db(squeeze(wavesraw.W_TRAN_p2_B(:,end,TickBins))),'.','Color',[0.7 0.7 0.7]);
plot(wavesraw.freq(ExBinsTick),db(squeeze(wavesraw.W_TRAN_p2_B(:,end,ExBinsTick))),'+','Color',[0 0.7 0]);


%%
xlims = [0.8 1.2]*1e9;

figure(5196821)
clf
subplot(221)
plot(wavesraw.freq,squeeze(db(wavesraw.W_TRAN_p1_A)),'.')
title('p1 A')
xlim(xlims)

subplot(222)
plot(wavesraw.freq,squeeze(db(wavesraw.W_TRAN_p2_A)),'.')
title('p2 A')
xlim(xlims)

subplot(223)
plot(wavesraw.freq,squeeze(db(wavesraw.W_TRAN_p1_B)),'.')
title('p1 B')
xlim(xlims)

subplot(224)
plot(wavesraw.freq,squeeze(db(wavesraw.W_TRAN_p2_B)),'.')
title('p2 B')
xlim(xlims)


%%

% [BLAs,spec,waves,specraw,wavesraw] = Zipper_Analysis('classCnetlist.net',MSdef,'StagesToEstimate',{'TRAN'},...
%     'numberOfRealisations',20,'TicklerAmplitudes',1e-6,'TicklerDistance',1,'plot',true);
% LIN = Linear_Analysis_v2('classCnetlist.net' , 'fstart', MSdef.fmin , 'fstop', MSdef.fmax , 'fstep', MSdef.f0,'MSdef',MSdef);

RES = DCA('classCnetlist.net','MSdef',MSdef,'BLAtechnique','ZIPPER','display',false,'numberOfRealisations',20);

LIN = DCA('classCnetlist.net','MSdef',MSdef,'BLAtechnique','Sparam','display',false,'numberOfRealisations',20);



        
%% plot the B2 wave

% MSbins are the bins of the main multisine
MSbins = 1191 : 3 : 1311;
% Tbins are the tickler bins
TBins = 1192 : 6 : 1312;
% disto is the distortion generated by the main multisine
disto = (950:3:1550)+1;
% range is the whole bin range that will be plotted
range = 950 : 1550;
% rest contains all the leftover bins
rest = setdiff(range,[MSbins TBins disto]);

% toplot are the realisations that will be plotted
toplot = [2 : 9];

wavetoplot = 20*log10(abs(RES.wavesraw.W_TRAN_p2_B(:,1,:)))+30;

figure(101)
clf
% plot(wavesraw.freq/1e9,db(squeeze(wavesraw.W_TRAN_p2_B([6 7 8 9],1,:))),'+');
plot( RES.wavesraw.freq(disto )/1e9 , squeeze(wavetoplot(toplot,1,disto )),'r+');
hold on
plot( RES.wavesraw.freq(rest  )/1e9 , squeeze(wavetoplot(toplot,1,rest  )),'b.','MarkerSize',2);
plot( RES.wavesraw.freq(MSbins)/1e9 , squeeze(wavetoplot(toplot,1,MSbins)),'k+');
plot( RES.wavesraw.freq(TBins )/1e9 , squeeze(wavetoplot(toplot,1,TBins )),'+','Color',[0 150/255 0]);

xlim([0.9 1.1]);
ylim([-140 -0]);
set(gca,'XTick',[0.9 0.98 1.02 1.1])
set(gca,'YTick',[-80 0])
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','Interpreter','none');
ylabel('Amplitude $\left[ \mathrm{dBm} \right]$','Interpreter','none');
grid on

% matlab2tikz('ex3_B2wave.tex','height','\figureheight','width','\figurewidth','parseStrings',false);


%%
figure(2)
clf
cols = {[1 0 0],[0 150/255 0],[0 0 1],[0 0 0]};
ind = 1;
ylims = [0 3.6];
xlims = [0.98 1.02];
for uu=1:2
    for yy=1:2
        subplot(2,2,ind)
        % plot the errorbars of the BLA
        errorbar( RES.BLAs.freq/1e9 , abs(squeeze(RES.BLAs.TRAN.G(uu,yy,:))),...
            3*abs(sqrt(squeeze(RES.BLAs.TRAN.varG(uu,yy,:)))),'-','Color',cols{ind});
        hold on
        % replot the BLA with a fatter line
        plot(RES.BLAs.freq/1e9 , abs(squeeze(RES.BLAs.TRAN.G(uu,yy,:))),'-','Color',cols{ind},'LineWidth',2);
        % plot the small-signal frf as well
        plot(LIN.BLAs.freq/1e9,abs(squeeze(LIN.BLAs.TRAN.G(uu,yy,:))),'--','Color',cols{ind},'LineWidth',2);
        switch ind
            case 1
                ylabel('Magnitude $\left[ \mathrm{abs} \right]$','interpreter','none');
                ylim([0.95 0.98]);
                set(gca,'YTick',[0.95 0.965 0.98])
                set(gca,'XTick',[0.98 1 1.02],'XTickLabel',{})
                text(0.98,0.95,['$S_{' num2str(uu) num2str(yy) '}$'],'Color',cols{ind},'interpreter','none','VerticalAlignment','bottom','BackgroundColor',[1 1 1])
            case 2
                ylim([0.08 0.12])
                set(gca,'YTick',[0.08 0.10 0.12])
                set(gca,'XTick',[0.98 1 1.02],'XTickLabel',{});
                text(0.98,0.08,['$S_{' num2str(uu) num2str(yy) '}$'],'Color',cols{ind},'interpreter','none','VerticalAlignment','bottom','BackgroundColor',[1 1 1])
            case 3
                ylabel('Magnitude $\left[ \mathrm{abs} \right]$','interpreter','none');
                ylim([1.6 3.6]);
                set(gca,'YTick',[1.6 2.6 3.6])
                set(gca,'XTick',[0.98 1 1.02])
                xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none');
                text(0.98,1.8,['$S_{' num2str(uu) num2str(yy) '}$'],'interpreter','none','Color',cols{ind},'VerticalAlignment','bottom','BackgroundColor',[1 1 1])
            case 4
                ylim([0.5 1]);
                set(gca,'YTick',[0.5 0.75 1])
                set(gca,'XTick',[0.98 1 1.02])
                xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none');
                text(0.98,0.5,['$S_{' num2str(uu) num2str(yy) '}$'],'interpreter','none','Color',cols{ind},'VerticalAlignment','bottom','BackgroundColor',[1 1 1])
        end
        ind=ind+1;
        xlim(xlims);
    end
end

% matlab2tikz('ex3_BLA.tex','height','2.3\figureheight','width','\figurewidth','parseStrings',false);

%% check the quality of the BLA for both the small-signal and for the BLA

figure(555)
clf
hold on
plot(LIN.SIMO.Measured.freq/1e9  , db(LIN.SIMO.Measured.W_TRAN_p2_B) ,'+-','Color',[0 0.7 0],'linewidth',2);
plot(LIN.SIMO.Predicted.freq/1e9 , db(LIN.SIMO.Predicted.W_TRAN_p2_B) ,'-','Color',[1 0.7 0],'linewidth',2);
plot(RES.SIMO.Predicted.freq/1e9 , db(RES.SIMO.Predicted.W_TRAN_p2_B) ,'-','Color',[0 0.7 1],'linewidth',2);
grid on
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none');
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none');
xlim([MSdef.fmin MSdef.fmax]/1e9)
set(gca,'XTick',[0.98 1 1.02])
legend({'Measured','Predicted with linear','Predicted with the BLA'},'Location','E')

cleanfigure
matlab2tikz('ex3_BLAtest.tex','height','\figureheight','width','\figurewidth','parseStrings',false);
