% in this script I play with the filter in the stable/unstable projection
% to look at its influence
clear all
close all
clc

addpath(genpath('C:\Users\Adam\Google Drive\Notities\Stability Hardy\Code Fabien'));

% generate the data

datasource = 'unstable_ex.mat';
% datasource = 'NEW';

if ~strcmpi(datasource,'NEW')
    disp('loading data')
    load(datasource);
else
    disp('generating new data')
    % N is the amount of stable poles in the random system
    N = 50;
    % I generate a random system without a perfect integrator
    p={0};
    while any(db(p{1})<-200)
        % generate a random system of very high order
        sys = rss(N);
        % check whether there's a perfect integrator present in the system,
        % if there is, just retry
        [z,p,k] = zpkdata(sys);
    end
    % if you want an unstable system, set this variable to true
    unstable = true;
    if unstable
        unst_real = 0.4;     unst_imag = 1*2*pi;
        % add in the unstable poles. If the imaginary part is zero, just add a
        % single pole on the real axis
        if unst_imag==0
            p = {[p{1};unst_real]};
        else
            p = {[p{1};unst_real+unst_imag*1i;unst_real-unst_imag*1i]};
        end
        % and regenerate the system with the new poles
        sys = zpk(z,p,k);
    end
    % add some delay to the input and the output, to maks stuff more difficult
    sys.OutputDelay = 0;
    sys.InputDelay = 0;
    % plot the pole-zero map
    figure(3)
    pzmap(sys);
    % fmax is the maximum frequency simulated
    fmax = 50;
    % the comp frequency grid is compensated in advance such that a
    % linear grid is obtained on the unit disc. This avoids the need
    % for a linear interpolation to use the FFT
    % 2^N points on the unit circle will be used
    f=linspace(0,fmax,10000);
    % get the frequency response of that system on different frequencies
    Z = squeeze(freqresp(sys,f,'Hz'));
    % set calcerror to true if you want to calculate the error
    calcError = false;
end

% perform the projection
[ystable,yunstable,fs,fus,poles,freq,Z_filtered,args] = ...
    StableUnstableProjection(Z,f,'Filtertype','LOWPASS',...
    'plot',false,'estimatePoles',false);

% plot the pzmap of the system
figure(1)
clf
[z,p,k]=zpkdata(sys);
hold on
plot(real(z{1})/2/pi,imag(z{1})/2/pi,'ko');
plot(real(p{1})/2/pi,imag(p{1})/2/pi,'kx');
plot(real(poles),imag(poles),'rs');
xlim([-1 0.25]);
grid on
xlabel('Real part $\left[ \mathrm{Hz} \right]$','interpreter','none');
ylabel('Imaginary part $\left[ \mathrm{Hz} \right]$','interpreter','none');
set(gca,'XTick',[-1 -0.5 0 1]);

matlab2tikz('rss_example_pzmap.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

%% plot the original impedance, in amplitude and phase

figure(5)
clf
subplot(121)
hold on
plot(f,db(Z),'LineWidth',1,'Color',[0 100/255 0]);
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right] $','interpreter','none')
legend({'$Z_\mathrm{original}$'},'interpreter','none')
grid on
ylim([-70 0])
xlim([0 max(f)])
set(gca,'YTick',[-50 -25 0])
subplot(122)
hold on
plot(f,(angle(Z))/pi,'LineWidth',1,'Color',[0 100/255 0]);
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right] $','interpreter','none')
set(gca,'YTick',[-1 0 1],'YTickLabel',{'$-\pi$','$0$','$\pi$'})
grid on
% ylim([-30 0])
xlim([0 max(f)])


cleanfigure
matlab2tikz('rss_example_impedance.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



%% plot the filter frequency response

figure(45213)
clf
subplot(121)
plot(f,abs(args.filter),'LineWidth',1,'Color',[0 0.7 0]);
xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \norm{ \cdot } \right]$','interpreter','none')
grid on
xlim([0 max(f)])
subplot(122)
plot(f,angle(args.filter)/pi,'LineWidth',1,'Color',[0 0.7 0]);
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right] $','interpreter','none')
set(gca,'YTick',[-1 0 1],'YTickLabel',{'$-\pi$','$0$','$\pi$'},'ticklabelinterpreter','none')
grid on
xlim([0 max(f)])


cleanfigure
matlab2tikz('rss_example_filter.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%% plot the mapping from f to theta and the input of the FFT

figure(093123)
clf

plot(args.theta_fft/pi,db(ifft(ifftshift([fs fus]))*length(args.theta_fft)),'LineWidth',1,'Color',[0 100/255 0]);
set(gca,'XTick',[0 0.25 1 1.75 2],'XTickLabel',{'$0$','$\frac{\pi}{4}$','$\pi$','$\frac{3\pi}{4}$','$2\pi$' },'ticklabelinterpreter','none')
xlabel('$\theta$  $\left[ \mathrm{rad} \right] $','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right] $','interpreter','none')
grid on
ylim([-70 0])
xlim([0 2])


cleanfigure
matlab2tikz('rss_example_fftinput.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%% plot the coefficients

C = length(fs);
% coeffrange = max(abs([fs fus]));
% fun = @(x) tanh(2*pi*x/coeffrange);
% tikz = linspace(-coeffrange,coeffrange,11);
% Yticks = fun(tikz);
fun = @(x) x;

figure(909823)
clf
hold on
plot(0:C-1,fliplr(fun(fs)),'b.','markersize',1)
plot(-C:-1,fliplr(fun(fus)),'r.','markersize',1)
xlabel('base function coefficient $b$','interpreter','none')
ylabel('Coefficient value')
% set(gca,'Ytick',Yticks)
legend({'Stable Coefficients','Unstable coefficients'},'interpreter','none')
grid on

cleanfigure
matlab2tikz('rss_example_coeffs.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%% plot the results of the projection
figure(10)
% clf
subplot(121)
hold on
plot(f,db(Z_filtered),'LineWidth',1,'Color',[0 100/255 0]);
plot(freq,db(ystable),'b-','LineWidth',1);
plot(freq,db(yunstable),'r-','LineWidth',1);
% plot(freq,db(yunstable+ystable),'k-','LineWidth',1);
ylim([-70 0])
xlim([0 max(f)])
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right] $','interpreter','none')
legend({'$Z_\mathrm{f}$','$Z_\mathrm{stable}$','$Z_\mathrm{unstable}$'},'interpreter','none')
grid on

subplot(122)
hold on
plot(f,(angle(Z_filtered))/pi,'LineWidth',1,'Color',[0 100/255 0]);%
plot(freq,(angle(ystable))/pi,'b-','LineWidth',1);%
plot(freq,(angle(yunstable))/pi,'r-','LineWidth',1);% 
% ylim([-30 1])
set(gca,'YTick',[-1 0 1],'YTickLabel',{'$-\pi$','$0$','$\pi$'},'ticklabelinterpreter','none')
xlim([0 max(f)])
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right] $','interpreter','none')
grid on

cleanfigure
matlab2tikz('rss_example_result.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%% perform vector fitting on the unstable part

[SER,poles,rmserr,fit,opts] = VF(yunstable,2*pi*1i*freq,[],'N',2,'stable',false,'asymp',1,'plot',false);

figure(76231)
clf
subplot(121)
hold on
plot(freq,db(yunstable),'r-')
plot(freq,db(fit),'m--')
xlim([0 max(f)])
grid on
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right] $','interpreter','none')
legend('Unstable part','fit of order 2')
subplot(122)
hold on
plot(freq,angle(yunstable)/pi,'r-')
plot(freq,angle(fit)/pi,'m--')
xlim([0 max(f)])
grid on
set(gca,'YTick',[-1 -0.5 0 0.5],'YTickLabel',{'$-\pi$','$-\frac{\pi}{2}$','$0$','$\frac{\pi}{2}$'},'ticklabelinterpreter','none')
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right] $','interpreter','none')

cleanfigure
matlab2tikz('rss_example_vectorfit.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%% Also show that we can zoom in on an instability

% perform the projection
[ystable_zoom,yunstable_zoom,~,~,~,freq_zoom,Z_filtered_zoom,args] = ...
    StableUnstableProjection(Z,f,'plot',false,'estimatePoles',false,...
    'Filtertype','BANDPASS','filtermin',0.4/5,'filtermax',0.6/5);

figure(101)
plot(args.f,db(args.filter));

figure(20)
clf
subplot(121)
hold on
plot(f,(db(Z_filtered_zoom))/pi,'LineWidth',1,'Color',[0 100/255 0]);
plot(freq_zoom,(db(ystable_zoom))/pi,'b-','LineWidth',1);
plot(freq_zoom,(db(yunstable_zoom))/pi,'r-','LineWidth',1);
ylim([-20 0])
xlim([0 max(f)])
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right] $','interpreter','none')
legend({'$Z_\mathrm{f}$','$Z_\mathrm{stable}$','$Z_\mathrm{unstable}$'},'interpreter','none')
grid on
set(gca,'YTick',[-20 -10 0])

subplot(122)
hold on
plot(f,(angle(Z_filtered))/pi,'LineWidth',1,'Color',[0 100/255 0]);%
plot(freq,(angle(ystable))/pi,'b-','LineWidth',1);%
plot(freq,(angle(yunstable))/pi,'r-','LineWidth',1);% 
% ylim([-30 1])
set(gca,'YTick',[-1 0 1],'YTickLabel',{'$-\pi$','$0$','$\pi$'},'ticklabelinterpreter','none')
xlim([0 max(f)])
xlabel('Frequency $\left[ \mathrm{Hz} \right] $','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right] $','interpreter','none')
grid on

cleanfigure
matlab2tikz('rss_example_zoomed.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



