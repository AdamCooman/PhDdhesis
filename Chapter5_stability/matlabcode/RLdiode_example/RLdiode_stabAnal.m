% In this script, I apply the stablility analysis to a Balanced amplifier.
% 
% The details about the schematic used to determine the impedances of the
% amplifier are shown in the attached pdf file
%
% 
clear variables
close all
clc

addpath(genpath('C:\Users\Adam\Google Drive\Notities\Stability Hardy\Code Fabien\'));

% The simulation results have been obtained with an LSSS simulation on a
% linear frequency grid between 1kHz and 2MHz with steps of 1kHz
% The AC current source was connected to the diode in the circuit and the
% voltage was measured at the diode itself.
% 7 Harmonic Transfed Functions were saved: -3, -2, -1, 0, 1, 2, 3
% so the resulting matrix of data is 7x2000. HTF0 can be found at Z(4,:)

% First analyse the low amplitude data. Here, the orbit is stable
load('data\lowAmplData.mat');

% call the projection method
tic
[ystable,yunstable,~,~,~,freq_anal] = StableUnstableProjection(Z,freq,'plot',false);
disp(['The stability analysis took ' num2str(toc) ' seconds']);

% plot the results
figure(1)
clf
subplot(121)
hold on
plot(freq_anal/1e6,db(ystable(3,:)),'b-.','linewidth',1);
plot(freq_anal/1e6,db(ystable(4,:)),'b-','linewidth',1);
plot(freq_anal/1e6,db(ystable(5,:)),'b--','linewidth',1);
plot(freq_anal/1e6,db(yunstable(3,:)),'r-.','linewidth',1);
plot(freq_anal/1e6,db(yunstable(4,:)),'r-','linewidth',1);
plot(freq_anal/1e6,db(yunstable(5,:)),'r--','linewidth',1);
% title('Stability analysis of the low amplitude orbit','fontweight','normal')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')
xlim([0 0.5]);
ylim([40 90]);
set(gca,'XTick',[0:0.1:0.5]);
set(gca,'YTick',[40 60 80]);
grid on
legend({'$Z^{\left[ -1 \right]}_{dd}$','$Z^{\left[ 0 \right]}_{dd}$','$Z^{\left[ +1 \right]}_{dd}$'},'interpreter','none')

% title('\vspace{1cm}$V_\mathrm{in}=0.5\mathrm{V}$','fontweight','normal','interpreter','none');

subplot(122)
hold on
plot(freq_anal/1e6,unwrap(angle(ystable(3,:))),'b-.','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(ystable(4,:))),'b-','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(ystable(5,:))),'b--','linewidth',1);
% plot(freq_anal/1e6,unwrap(angle(yunstable(3,:))),'r-.','linewidth',1);
% plot(freq_anal/1e6,unwrap(angle(yunstable(4,:))),'r-','linewidth',1);
% plot(freq_anal/1e6,unwrap(angle(yunstable(5,:))),'r--','linewidth',1);
% title('Stability analysis of the low amplitude orbit','fontweight','normal')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right]$','interpreter','none')
xlim([0 0.5]);
% ylim([40 90]);
set(gca,'XTick',[0:0.1:0.5]);
% set(gca,'YTick',[40 60 80]);
grid on


cleanfigure
matlab2tikz('RLdiode_StabAnal_LowAmp.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



% Now load the high amplitude data. Here the orbit is unstable
load('data\highAmplData.mat');

% call the projection method
tic
[ystable,yunstable] = StableUnstableProjection(Z,freq,'plot',false);
disp(['The stability analysis took ' num2str(toc) ' seconds']);

% plot the results
figure(2)
subplot(121)
hold on
plot(freq_anal/1e6,db(ystable(3,:)),'b-.','linewidth',1);
plot(freq_anal/1e6,db(ystable(4,:)),'b-','linewidth',1);
plot(freq_anal/1e6,db(ystable(5,:)),'b--','linewidth',1);
plot(freq_anal/1e6,db(yunstable(3,:)),'r-.','linewidth',1);
plot(freq_anal/1e6,db(yunstable(4,:)),'r-','linewidth',1);
plot(freq_anal/1e6,db(yunstable(5,:)),'r--','linewidth',1);
% title('Stability analysis of the high amplitude orbit','fontweight','normal')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')
xlim([0 0.5]);
ylim([40 85]);
set(gca,'YTick',[40 60 80]);
set(gca,'XTick',[0:0.1:0.5]);
grid on
legend({},'interpreter','none')
legend({'$Z^{\left[ -1 \right]}_{dd}$','$Z^{\left[ 0 \right]}_{dd}$','$Z^{\left[ +1 \right]}_{dd}$'},'interpreter','none')

subplot(122)
hold on
plot(freq_anal/1e6,unwrap(angle(ystable(3,:))),'b-.','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(ystable(4,:))),'b-','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(ystable(5,:))),'b--','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(yunstable(3,:))),'r-.','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(yunstable(4,:))),'r-','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(yunstable(5,:))),'r--','linewidth',1);
% title('Stability analysis of the high amplitude orbit','fontweight','normal')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right]$','interpreter','none')
xlim([0 0.5]);
% ylim([40 85]);
% set(gca,'YTick',[40 60 80]);
set(gca,'XTick',[0:0.1:0.5]);
grid on


% title('\vspace{1cm}$V_\mathrm{in}=1.5\mathrm{V}$','fontweight','normal','interpreter','none');

cleanfigure
matlab2tikz('RLdiode_StabAnal_HighAmp.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

