clear all
close all
clc



setpref('ADS','parallelSims',3);

netlist = readTextFile('RLdiode.net');

% find the line in the netlist where Vin is specified
ind = find(cellfun(@(x)~isempty(x),regexp(netlist,'^Vin=')));


netlist{end+1} = ADSaddTran(-1,'TRAN1','StartTime','1000/F','StopTime','1030/F',...
    'MaxTimeStep','1/F/100','MinTimeStep','1/F/100','TimeStepControl','0');

VinTRAN = 0.01:0.01:3;

netlistNames = {};
for vv=1:length(Vin)
    netlist{ind} = sprintf('Vin = %s',eng(Vin(vv)));
    netlistNames{end+1} = ['tempNetlist' num2str(vv) '.net'];
    writeTextFile(netlist,netlistNames{end});
end

simRes = ADSrunSimulation(netlistNames);

% delete the netlists
for vv=1:length(netlistNames)
    delete(netlistNames{vv});
end

% gather the data from the simulation results
for ii=1:length(simRes);
    if ii==1
        fields = fieldnames(simRes{ii}.sim1_TRAN);
    end
    for ff=1:length(fields)
        if ~strcmp(fields{ff},'time')
            TRAN.(fields{ff})(ii,:) = simRes{ii}.sim1_TRAN.(fields{ff});
        end
    end
end
TRAN.time = simRes{ii}.sim1_TRAN.time;




%% now run a sweep with the HB simulator

netlist = readTextFile('RLdiode.net');

% find the line in the netlist where Vin is specified
ind = find(cellfun(@(x)~isempty(x),regexp(netlist,'^Vin=')));


netlist{end+1} = 'HB:HB1 Freq=F Order=10';

VinHB = 0:0.1:1.5;

netlistNames = {};
for vv=1:length(VinHB)
    netlist{ind} = sprintf('Vin = %s',eng(VinHB(vv)));
    netlistNames{end+1} = ['tempNetlist' num2str(vv) '.net'];
    writeTextFile(netlist,netlistNames{end});
end

simRes = ADSrunSimulation(netlistNames);

% delete the netlists
for vv=1:length(netlistNames)
    delete(netlistNames{vv});
end

clear HB
% gather the data from the simulation results
for ii=1:length(simRes);
    if ii==1
        fields = fieldnames(simRes{ii}.sim1_HB);
    end
    for ff=1:length(fields)
        if ~strcmp(fields{ff},'freq')
            HB.(fields{ff})(ii,1,:) = simRes{ii}.sim1_HB.(fields{ff});
        end
    end
end
HB.freq = simRes{ii}.sim1_HB.freq;

[signal,time]=ADSconvert2timeDomain(HB,'oversample',100);

figure(2)
plot(time,squeeze(signal.I_d(:,1,:)));


%% also perform a HB simulation with a sub-harmonic

netlist = readTextFile('RLdiode.net');

% find the line in the netlist where Vin is specified
ind = find(cellfun(@(x)~isempty(x),regexp(netlist,'^Vin=')));

% the base frequency of the HB is halved, the order is double
netlist{end+1} = 'HB:HB1 Freq=F/2 Order=20';
% we generate the initial values with a transient simulation
netlist{end+1} = 'Tran:HB2_tran HB_Sol=1 SteadyState=1 Freq[1]=F/2 Order[1]=20 OutputPlan="HB2_Output" SteadyStateMinTime=500/F StopTime=1000/F MaxTimeStep=1/F/100';
netlist{end+1} = 'Component:tahb_HB2 Module="ATAHB" Type="ModelExtractor"  Tran_Analysis="HB2_tran" HB_Analysis="HB1"';


VinHB2 = 0.88:0.02:2;

netlistNames = {};
for vv=1:length(VinHB2)
    netlist{ind} = sprintf('Vin = %s',eng(VinHB2(vv)));
    netlistNames{end+1} = ['tempNetlist' num2str(vv) '.net'];
    writeTextFile(netlist,netlistNames{end});
end

simRes = ADSrunSimulation(netlistNames);

% delete the netlists
for vv=1:length(netlistNames)
    delete(netlistNames{vv});
end

clear HB2
% gather the data from the simulation results
for ii=1:length(simRes);
    if ii==1
        fields = fieldnames(simRes{ii}.sim1_HB);
    end
    for ff=1:length(fields)
        if ~strcmp(fields{ff},'freq')
            HB2.(fields{ff})(ii,1,:) = simRes{ii}.sim1_HB.(fields{ff});
        end
    end
end
HB2.freq = simRes{ii}.sim1_HB.freq;

[signal2,time2]=ADSconvert2timeDomain(HB2,'oversample',100);


figure(2)
plot(time2,squeeze(signal2.I_d(:,1,:)));



%% we need to synchronise the HB signals with the TRAN signals
figure(3)
clf
plot(time,squeeze(signal.in(2:5,1,:)),'b-');
hold on
plot(TRAN.time-TRAN.time(1),TRAN.in(2:5,:),'r');




%% generate the bifurcation plot

% I put the data in a vector first, so that cleanfigure can deal with it
% easily
data = TRAN.I_d(:,1:100:end);
xdata = repmat(VinTRAN.',[1 31]);

% put the frequency-doubled points in a vector as well
data2 = vec(squeeze(signal2.I_d(:,1,[1 length(time2)/2])));
xdata2 = vec(repmat(VinHB2.',[1 2]));

% make 4 groups out of the data2
group1  = data2(data2>0.5/1000);
xgroup1 = xdata2(data2>0.5/1000);
[xgroup1,I] = sort(xgroup1);
group1 = group1(I);

group2 = data2((data2<0.5/1000)&(data2>0.11/1000));
xgroup2 = xdata2((data2<0.5/1000)&(data2>0.11/1000));
[xgroup2,I] = sort(xgroup2);
group2 = group2(I);

figure(1)
clf
hold on
plot(VinHB,1000*signal.I_d(:,1,1),'-','Color',[200 200 200]/255);
plot(xgroup1,1000*group1,'r-','Color',[200 200 200]/255);
plot(xgroup2,1000*group2,'r-','Color',[200 200 200]/255);

% plot(VinHB2,1000*squeeze(signal2.I_d(:,1,[1 length(time2)/2])),'+','Color',[200 200 200]/255);
plot(vec(xdata),1000*vec(data),'.','markersize',3)

xlabel('Amplitude at the input $\left[ \mathrm{V} \right]$','interpreter','none');
ylabel('Current at $t=k*10\mu s \left[ \mathrm{mA} \right]$','interpreter','none');
set(gca,'XTick',[0 1 2 3]);
set(gca,'YTick',[0 0.5 1]);

cleanfigure();
matlab2tikz('bifurcation_diagram.tex','width','\figurewidth','height','\figureheight','parseStrings',false);



