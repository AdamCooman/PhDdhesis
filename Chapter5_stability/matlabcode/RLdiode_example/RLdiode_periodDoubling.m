clear all
close all
clc

AMPL = 1.2;
periodDoubling = true;
netlist = readTextFile('RLdiode_nosource.net');

% generate the multisine
MSdef = MScreate(100e3,100e3,'ampl',AMPL);
MSdef.MSnode = 'in';

% set the HB settings
HBsettings = calculateHBSettings(MSdef,10);
if periodDoubling
    % when periodDoubling is acive, solve HB at half the frequency
    HBsettings.Freq = HBsettings.Freq/2;
    HBsettings.Order = HBsettings.Order*2;
    HBsettings.TAHB_Enable=1;
    HBsettings.HBAHB_Enable=0;
    % add TRAN statement to the netlist to get initial values
    netlist{end+1} = ADSaddTran(-1,'HBTRAN','HB_Sol',1,'SteadyState',1,...
        'Freq',HBsettings.Freq,'Order',HBsettings.Order,...
        'SteadyStateMinTime',1000/HBsettings.Freq,'StopTime',1030/HBsettings.Freq,...
        'MaxTimeStep',1/HBsettings.Freq/100);
    netlist{end+1} = 'Component:TAHB Module="ATAHB" Type="ModelExtractor" Tran_Analysis="HBTRAN" HB_Analysis="HARMONICBALANCE_sim"   OutputTranDataTAHB=no ';
end

% run the LSSS simulation
[LSSS,HB] = ADSsimulateLSSS(netlist,MSdef,'LSSSNode','Vd',...
    'Start',1e3+1,'Stop',2e6+1,'Step',1e3,...
    'SimSettingsStruct',HBsettings,'cleanupFiles',false);

% extract the HTFs
HTFs = LSSSextractHTFs(LSSS,'cos');

Z = squeeze(HTFs.Vd(1,:,:));
freq = HTFs.freq;

% FNAME = ['RLdiode_' eng(Amp) 'V_' eng(HBsettings.Freq) 'Hz.mat'];

% save(FNAME,'Z','freq','HB','')

% do some plottin'

figure(31463212)
plot(HTFs.freq,squeeze(db(HTFs.Vd(1,:,:))))



%% check the stability

addpath(genpath('C:\users\adam.cooman\Google Drive\notities\stability Hardy\code Fabien\'));


[ystable,yunstable,fs,fus,poles,freq_anal,Z_filtered,args] = ...
    StableUnstableProjection(squeeze(HTFs.Vd(1,:,:)),HTFs.freq);

%% generate the plot for the thesis


% plot the results
figure(2)
clf

subplot(121)
hold on
plot(freq_anal/1e6,db(ystable(3,:)),'b-.','linewidth',1);
plot(freq_anal/1e6,db(ystable(4,:)),'b-','linewidth',1);
plot(freq_anal/1e6,db(ystable(5,:)),'b--','linewidth',1);
plot(freq_anal/1e6,db(yunstable(3,:)),'r-.','linewidth',1);
plot(freq_anal/1e6,db(yunstable(4,:)),'r-','linewidth',1);
plot(freq_anal/1e6,db(yunstable(5,:)),'r--','linewidth',1);
% title('Stability analysis of the high amplitude orbit','fontweight','normal')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Magnitude $\left[ \mathrm{dB} \right]$','interpreter','none')
xlim([0 0.5]);
ylim([20 60]);
set(gca,'YTick',[20 40 60]);
set(gca,'XTick',[0:0.1:0.5]);
grid on
legend({},'interpreter','none')
legend({'$Z^{\left[ -1 \right]}_{dd}$','$Z^{\left[ 0 \right]}_{dd}$','$Z^{\left[ +1 \right]}_{dd}$'},'interpreter','none')

subplot(122)
hold on
plot(freq_anal/1e6,unwrap(angle(ystable(3,:))),'b-.','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(ystable(4,:))),'b-','linewidth',1);
plot(freq_anal/1e6,unwrap(angle(ystable(5,:))),'b--','linewidth',1);
% plot(freq_anal/1e6,unwrap(angle(yunstable(3,:))),'r-.','linewidth',1);
% plot(freq_anal/1e6,unwrap(angle(yunstable(4,:))),'r-','linewidth',1);
% plot(freq_anal/1e6,unwrap(angle(yunstable(5,:))),'r--','linewidth',1);
% title('Stability analysis of the high amplitude orbit','fontweight','normal')
xlabel('Frequency $\left[ \mathrm{MHz} \right]$','interpreter','none')
ylabel('Phase $\left[ \mathrm{rad} \right]$','interpreter','none')
xlim([0 0.5]);
% ylim([40 85]);
% set(gca,'YTick',[40 60 80]);
set(gca,'XTick',[0:0.1:0.5]);
grid on


% title('\vspace{1cm}$V_\mathrm{in}=1.5\mathrm{V}$','fontweight','normal','interpreter','none');

cleanfigure
matlab2tikz('RLdiode_StabAnal_Doubling.tex','width','\figurewidth','height','\figureheight','parseStrings',false);

%% also plot the period-doubled solution

[hb,time] = ADSconvert2timeDomain(HB);

figure(1658132)
clf
hold on
plot(time*100000,squeeze(hb.in),'linewidth',1);
plot(time*100000,squeeze(hb.Vd),'color',[0 100/255 0],'linewidth',1);
grid on
ylabel('Voltage $\left[ \mathrm{V} \right]$','interpreter','none')
xlabel('Time $\left[ \mathrm{\mu s} \right]$','interpreter','none')
legend({'$V_\mathrm{in}$','$V_\mathrm{diode}$'},'interpreter','none','location','SE')

cleanfigure
matlab2tikz('RLdiode_StabAnal_Doubling_solution.tex','width','\figurewidth','height','\figureheight','parseStrings',false);
