clear all
close all
clc

Iex = ADSimportSimData('STAB_I.mat');
Vex = ADSimportSimData('STAB_V.mat');

% determine the A B C and D parameters
B = Vex.sim1_AC.I_If;
D = Vex.sim1_AC.Ve;
A = Iex.sim1_AC.I_If;
C = Iex.sim1_AC.Ve;

freq = Iex.sim1_AC.freq;

% determine the return ratio
T = (2*(A.*D-B.*C) - A + D)./(2*(B.*C-A.*D) + A - D + 1);

xlims = [100 1e8];

figure
subplot(121)
semilogx(freq,db(T))
grid on
set(gca,'YTick',[-60 0 60])
xlabel('Frequency')
ylabel('Loopgain')
xlim(xlims)
subplot(122)
semilogx(freq,unwrap(angle(T))/2/pi*360)
grid on
set(gca,'YTick',[-180 -90 0])
xlim(xlims)
xlabel('Frequency')
ylabel('Loopgain')

cleanfigure
matlab2tikz('RR.tex','standalone',true)