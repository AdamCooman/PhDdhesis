clc
clear variables
close all

addpath('C:\Users\Adam\Google Drive\Notities\Stability Hardy\Code Fabien');

RES = ADSimportSimData('LNA.mat');
% 
% Z = RES.sim1_AC.coll;
% f = RES.sim1_AC.freq;

f = linspace(0,RES.sim1_AC.freq(end),100000);
Z = spline(RES.sim1_AC.freq,RES.sim1_AC.coll,f);


[ystable,yunstable,~,~,poles,freq,Z_filtered] = ...
    StableUnstableProjection(Z,f,'plot',false,'FilterType','LOWPASS','FilterOrder',10,'FourierMethod','FFT');

figure(1)
clf
subplot(121)
hold on
plot(f/1e9,db(Z),'Color',[0 100/255 0],'linewidth',1);
plot(freq/1e9,db(ystable),'b','linewidth',1)
plot(freq/1e9,db(yunstable),'r','linewidth',1)
xlim([0 max(f)]/1e9)
ylim([-70 40]);
set(gca,'YTick',[-40 0 40])
set(gca,'XTick',0:2:10)
xlabel('Frequency $\left[ \mathrm{GHz} \right]$');
ylabel('Magnitude $\left[ \mathrm{dB}\left( \Omega \right) \right]$','interpreter','none');
grid on
legend({'$Z$','$Z_\mathrm{stable}$','$Z_\mathrm{unstable}$'})

subplot(122)
hold on
plot(f/1e9,unwrap(angle(Z))/pi,'Color',[0 100/255 0],'linewidth',1);
plot(freq/1e9,unwrap(angle(ystable))/pi,'b','linewidth',1)
plot(freq/1e9,unwrap(angle(yunstable))/pi,'r','linewidth',1)
xlim([0 max(f)]/1e9);
ylim([-4.5 1]);
ylabel('Phase $\left[ \mathrm{rad} \right]$','interpreter','none');
xlabel('Frequency $\left[ \mathrm{GHz} \right]$','interpreter','none');
set(gca,'XTick',0:2:10)
set(gca,'YTick',[-4 -2 0],'YTickLabel',{'$-4\pi$','$-2\pi$','$-0\pi$'})
grid on

cleanfigure
matlab2tikz('lna.tex','width','\figurewidth','height','\figureheight','parseStrings',false);


%%

% MSdef = MScreate(1,1,'Ampl',0,'Rout',50);
% MSdef.MSnode = 'N__6';
% [StabRes,Z,freq,leg,SIM] = ADScheckStability('lna_lumped.net',MSdef,...
%     'nodes',{'n2'},'excludeNodes',{'n1','n3','BFP_10x2E_55_'}...
%     ,'Start',0.0001,'Stop',50e9,'Step',1e6,'FFTpoints',2^15);
% 
% [StabRes2] = ADScheckStability('lna_lumped.net',MSdef,...
%     'nodes',{'n2'},'excludeNodes',{'n1','n3','BFP_10x2E_55_'}...
%     ,'Start',0.0001,'Stop',50e9,'Step',1e6,'FFTpoints',2^11);
% 
% [StabRes3] = ADScheckStability('lna_lumped.net',MSdef,...
%     'nodes',{'n2'},'excludeNodes',{'n1','n3','BFP_10x2E_55_'}...
%     ,'Start',0.0001,'Stop',50e9,'Step',1e6,'FFTpoints',2^9);
% 
% close all

%%
% clc

% figure(101)
% clf
% semilogx(freq,db(Z([2],:)));
% ylim([0 60])
% xlim([1e5 max(freq)]);
% % legend(leg([4 6 11 13]))
% ylabel('Impedance $\left[ \mathrm{dB} (\Omega) \right]$','interpreter','none')
% xlabel('Frequency $\left[ \mathrm{Hz} \right]$','interpreter','none')
% % set(gca,'XTick',[0 5 10 15]);
% set(gca,'YTick',[0 30 60]);
% grid on
% % cleanfigure
% % matlab2tikz('..\..\pics\lna_impedances.tex','width','\figurewidth','height','\figureheight','parseStrings',false);
% 
% % 
% figure(1)
% clf
% % subplot(211)
% plot(freq(1:10:end)/1e9,db(StabRes.Z_filtered(1,1:10:end)),'-','Color',[0 100/255 0]);
% hold on
% plot(StabRes.freq(1:10:end)/1e9,db(StabRes.ystable(1,1:10:end)),'b--','linewidth',1.5);
% plot(StabRes.freq(1:10:end)/1e9,db(StabRes.yunstable(1,1:10:end)),'r-.');
% plot(StabRes2.freq/1e9,db(StabRes2.yunstable),'r-.');
% plot(StabRes3.freq/1e9,db(StabRes3.yunstable),'r-.');
% ylim([-100 50]);
% xlim([0 50]);
% set(gca,'XTick',[0 10 20 30 40 50]);
% set(gca,'YTick',[-50 0 50]);
% grid on
% ylabel('Impedance $\left[\mathrm{dB}\left( \Omega \right)\right]$','interpreter','none');
% xlabel('Frequency $\left[\mathrm{GHz}\right]$','interpreter','none');
% 
% 
% text(20,-15,'{\color{red}$2^{9}$ points}','interpreter','none');
% text(20,-45,'{\color{red}$2^{11}$ points}','interpreter','none');
% text(20,-85,'{\color{red}$2^{15}$ points}','interpreter','none');
% 
% cleanfigure
% matlab2tikz('..\..\pics\lna_projection.tex','width','\figurewidth','height','\figureheight','parseStrings',false);
% 

