<ADSWorkspace Revision="41" Version="100">
    <Workspace Name="">
        <Library Name="ads_standard_layers" />
        <Library Name="adstechlib" />
        <Library Name="ads_schematic_layers" />
        <Library Name="empro_standard_layers" />
        <Library Name="ads_standard_layers_ic" />
        <Library Name="ads_schematic_layers_ic" />
        <Library Name="ads_schematic_ports_ic" />
        <Library Name="ads_rflib" />
        <Library Name="ads_sources" />
        <Library Name="ads_simulation" />
        <Library Name="ads_tlines" />
        <Library Name="ads_bondwires" />
        <Library Name="ads_datacmps" />
        <Library Name="ads_behavioral" />
        <Library Name="ads_textfonts" />
        <Library Name="ads_common_cmps" />
        <Library Name="ads_designs" />
        <Library Name="ads_verification_test_bench" />
        <Library Name="ThesisExamples_lib" />
        <Log Name="netlist.log" />
        <Log Name="search_history.log" />
        <Preferences Name="layout.prf" />
        <Preferences Name="schematic.prf" />
        <LibraryDefs Name="lib.defs" />
        <ConfigFile Name="de_sim.cfg" />
        <ConfigFile Name="hpeesofsim.cfg" />
        <Folder Name="Chapter5_Stability">
            <Cell Name="ThesisExamples_lib:Ex5.5_RLdiode_HB" />
            <Cell Name="ThesisExamples_lib:Ex5.5_RLdiode_TRAN" />
            <Cell Name="ThesisExamples_lib:Ex5.3_Balanced_Amplifier" />
            <Cell Name="ThesisExamples_lib:Ex5.1_Low_Noise_Amplifier" />
            <Folder Name="Auxiliary">
                <Cell Name="ThesisExamples_lib:bfp520" />
            </Folder>
            <Cell Name="ThesisExamples_lib:Ex5.5_RLdiode_HB_doubling" />
        </Folder>
        <Folder Name="Chapter2_Introduction" />
        <Folder Name="Chapter3_MIMO_BLA">
            <Cell Name="ThesisExamples_lib:BLA_classC_example" />
        </Folder>
        <Folder Name="Chapter4_DCA">
            <Cell Name="ThesisExamples_lib:Ex4.3_Miller_package_sparam" />
            <Cell Name="ThesisExamples_lib:Ex4.2_Two_SISO_stages_in_feedback" />
            <Folder Name="Auxiliary">
                <Cell Name="ThesisExamples_lib:EXP_block" />
                <Cell Name="ThesisExamples_lib:TANH_block" />
            </Folder>
        </Folder>
        <Dataset Name="RLdiode_TRAN.ds" />
        <Dataset Name="Balanced_Amplifier.ds" />
        <Folder Name="Appendix1_MultisineSims">
            <Cell Name="ThesisExamples_lib:MStestCircuit" />
            <Data_Display Name="MStestCircuit.dds" />
            <Cell Name="ThesisExamples_lib:HB_saturation" />
        </Folder>
        <Folder Name="Appendix2_LSSS">
            <Cell Name="ThesisExamples_lib:LSSS_testcircuit" />
        </Folder>
        <Dataset Name="MStestCircuit.ds" />
        <ConfigFile Name="dds.cfg" />
    </Workspace>
</ADSWorkspace>
